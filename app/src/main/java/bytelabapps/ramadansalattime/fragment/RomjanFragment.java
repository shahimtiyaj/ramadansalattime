package bytelabapps.ramadansalattime.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bytelabapps.ramadansalattime.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class RomjanFragment extends Fragment {


    public RomjanFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_romjan, null);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("back",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("now","romjan");
        editor.apply();

        return v;

    }


}
