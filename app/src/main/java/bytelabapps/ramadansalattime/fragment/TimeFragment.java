package bytelabapps.ramadansalattime.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import bytelabapps.ramadansalattime.R;
import bytelabapps.ramadansalattime.activities.DivisionActivity;
import bytelabapps.ramadansalattime.helper.GetLocation;
import bytelabapps.ramadansalattime.helper.PrayTime;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimeFragment extends Fragment {


    public TimeFragment() {
        // Required empty public constructor
    }

    TextView fazr;
    TextView zuhr;
    TextView asr;
    TextView magrib;
    TextView isha;
    TextView sunrise;
    TextView sunset;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_time, container, false);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("division", Context.MODE_PRIVATE);
        String division = sharedPreferences.getString("district", "");

        GetLocation location = new GetLocation(sharedPreferences);

        double lat = (double) location.getLatitude();
        double lon = (double) location.getLongitude();

        TextView position = (TextView) view.findViewById(R.id.position);
        position.setText(division);

        fazr = (TextView) view.findViewById(R.id.fazr);
        sunrise = (TextView) view.findViewById(R.id.sunrise);
        zuhr = (TextView) view.findViewById(R.id.zuhr);
        asr = (TextView) view.findViewById(R.id.asr);
      //  sunset = (TextView) view.findViewById(R.id.sunset);
        magrib = (TextView) view.findViewById(R.id.magrib);
        isha = (TextView) view.findViewById(R.id.isha);

        getTime(lat, lon);

        CardView location_change = (CardView) view.findViewById(R.id.location_cv);
        location_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DivisionActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        return view;
    }

    public void getTime(double latitude, double longitude) {

        double timezone = (Calendar.getInstance().getTimeZone()
                .getOffset(Calendar.getInstance().getTimeInMillis()))
                / (1000 * 60 * 60);

        PrayTime prayers = new PrayTime();

        prayers.setTimeFormat(prayers.Time12);
        prayers.setCalcMethod(prayers.Makkah);
        prayers.setAsrJuristic(prayers.Hanafi);
        prayers.setAdjustHighLats(prayers.AngleBased);
        int[] offsets = {0, 0, 0, 0, 0, 0, 0}; // {Fajr,Sunrise,Dhuhr,Asr,Sunset,Maghrib,Isha}
        prayers.tune(offsets);

        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);

        ArrayList prayerTimes = prayers.getPrayerTimes(cal, latitude,
                longitude, timezone);

        fazr.setText(String.valueOf(prayerTimes.get(0)));
        sunrise.setText(String.valueOf(prayerTimes.get(1)));
        zuhr.setText(String.valueOf(prayerTimes.get(2)));
        asr.setText(String.valueOf(prayerTimes.get(3)));
       // sunset.setText(String.valueOf(prayerTimes.get(4)));
        magrib.setText(String.valueOf(prayerTimes.get(5)));
        isha.setText(String.valueOf(prayerTimes.get(6)));

    }

}
