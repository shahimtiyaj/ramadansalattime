package bytelabapps.ramadansalattime.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;

import bytelabapps.ramadansalattime.R;

/**
 * Created by Shahimtiyaj on 7/29/2015.
 */
public class NameFragment extends Fragment {



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_listview, null);


        ListView lv = (ListView) v.findViewById(R.id.mylistView);

        lv.setAdapter(new medicalAdapter(getActivity()));


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("back",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("now","name");
        editor.apply();


        return v;
    }

    class SingleRow {
        String titles;
        String Description;
        String Des;
        String descript;
        int images;

        SingleRow(String des, String titles, String description, String descript) {
            this.Description = description;
            this.titles = titles;
            this.Des = des;
            this.descript = descript;
          //  this.images = images;
        }

    }

    class medicalAdapter extends BaseAdapter {

        // String[] titles;
        // String[] Description;
        // int[] images;

        Context contex;

        ArrayList<SingleRow> myliListView;

        medicalAdapter(Context c) {
            contex = c;
            myliListView = new ArrayList<SingleRow>();

            Resources res = c.getResources();
            String[] des = res.getStringArray(R.array.no);
            String[] titles = res.getStringArray(R.array.arabic_name);
            String[] description = res.getStringArray(R.array.arabic_prounanciation);
            String[] descript = res.getStringArray(R.array.arabic_name_meaning);

//            int[] images = {R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name,
//                    R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name,
//                    R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name,
//
//                    R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name,
//                    R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name,
//                    R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name,
//
//                    R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name,
//                    R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name,
//                    R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name,
//
//                    R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name, R.drawable.name,
//                    R.drawable.name,
//                    R.drawable.name,
//
//
//            };

            for (int i = 0; i < 99; i++) {
                myliListView.add(new SingleRow(des[i], titles[i], description[i], descript[i]));
            }
            ;
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return myliListView.size();
        }

        @Override
        public Object getItem(int i) {
            // TODO Auto-generated method stub
            return myliListView.get(i);
        }

        @Override
        public long getItemId(int i) {
            // TODO Auto-generated method stub
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            LayoutInflater inflater = (LayoutInflater) contex
                    .getSystemService(contex.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.fragment_name_list, parent, false);

            TextView descriptions1 = (TextView) v.findViewById(R.id.textView);
            TextView titles = (TextView) v.findViewById(R.id.textView1);
            TextView descriptions = (TextView) v.findViewById(R.id.textView2);
            TextView descript = (TextView) v.findViewById(R.id.textView3);

          //  ImageView images = (ImageView) v.findViewById(R.id.imageView1);

            SingleRow temp = myliListView.get(i);
            descriptions1.setText(temp.Des);
            titles.setText(temp.titles);
            descriptions.setText(temp.Description);
            descript.setText(temp.descript);
           // images.setImageResource(temp.images);


            return v;
        }

    }



}
