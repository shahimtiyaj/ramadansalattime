package bytelabapps.ramadansalattime.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import bytelabapps.ramadansalattime.R;
import bytelabapps.ramadansalattime.activities.DetailsActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class SuraFragment extends Fragment {


    public SuraFragment() {
        // Required empty public constructor
    }

    private ListView myliListView;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_listview, container, false);



//        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("back",Context.MODE_PRIVATE);
//        final SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString("now","sura");
//        editor.apply();

        myliListView = (ListView) view.findViewById(R.id.mylistView);

        myliListView.setAdapter(new CustomAdapter(getContext()));
        myliListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent;
                String head;

                String str1, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, str13, str14, str15, str16, str17, str18, str19, str20, str21, str22, str23, str24, str25, str26, str27, str28, str29, str30, str31, str32, str33, str34, str35, str36, str37, str38, str39, str40, str41, str42, str43, str44, str45, str46, str47, str48, str49, str50, str51, str52, str53, str54, str55, str56, str57, str58, str59, str60, str61, str62, str63, str64, str65, str66, str67, str68, str69, str70, str71, str72, str73, str74, str75, str76, str77, str78, str79, str80, str81, str82, str83, str84, str85, str86, str87, str88, str89, str90, str91, str92, str93, str94, str95, str96, str97, str98, str99, str100, str101;

                switch (position) {
                    case 0:




                        head = "সূরা যিলযাল (ভূমিকম্প) সূরা-৯৯, মাক্কী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "إِذَا زُلْزِلَتِ الْأَرْضُ زِلْزَالَهَا (1) وَأَخْرَجَتِ الْأَرْضُ أَثْقَالَهَا (2) وَقَالَ الْإِنْسَانُ مَا لَهَا (3) يَوْمَئِذٍ تُحَدِّثُ أَخْبَارَهَا (4) بِأَنَّ رَبَّكَ أَوْحَى لَهَا (5) يَوْمَئِذٍ يَصْدُرُ النَّاسُ أَشْتَاتًا لِيُرَوْا أَعْمَالَهُمْ (6) فَمَنْ يَعْمَلْ مِثْقَالَ ذَرَّةٍ خَيْرًا يَرَهُ (7) وَمَنْ يَعْمَلْ مِثْقَالَ ذَرَّةٍ شَرًّا يَرَهُ (8)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = "(১) এযা ঝুলঝিলাতিল আরযু ঝিলঝা-লাহা (২) ওয়া আখরাজাতিল আরযু আছক্বা-লাহা (৩) ওয়া ক্বা-লাল ইনসা-নু মা লাহা? (৪) ইয়াওমাইযিন তুহাদ্দিছু আখবা-রাহা (৫) বেআন্না রববাকা আওহা লাহা (৬) ইয়াওমায়িযিইঁ ইয়াছদুরুন না-সু আশতা-তাল লেইউরাও আ‘মা-লাহুম (৭) ফামাইঁ ইয়া‘মাল মিছক্বা-লা যার্রাতিন খায়রাইঁ ইয়ারাহ (৮) ওয়ামাইঁ ইয়া‘মাল মিছক্বা-লা যার্রাতিন শার্রাইঁ ইয়ারাহ ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "(১) যখন পৃথিবী তার (চূড়ান্ত) কম্পনে প্রকম্পিত হবে। (২) যখন ভূগর্ভ তার বোঝাসমূহ উদ্গীরণ করবে। (৩) এবং মানুষ বলে উঠবে, এর কি হ’ল? (৪) সেদিন সে (তার উপরে ঘটিত) সকল বৃত্তান্ত বর্ণনা করবে। (৫) কেননা তোমার পালনকর্তা তাকে প্রত্যাদেশ করবেন। (৬) সেদিন মানুষ বিভিন্ন দলে প্রকাশ পাবে, যাতে তাদেরকে তাদের কৃতকর্ম দেখানো যায়। (৭) অতঃপর কেউ অণু পরিমাণ সৎকর্ম করলে তা সে দেখতে পাবে (৮) এবং কেউ অণু পরিমাণ অসৎকর্ম করলে তাও সে দেখতে পাবে।’";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);



                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);

                        getActivity().finish();

                        break;

                    case 1:


                        head = "সূরা ‘আদিয়াত (ঊর্ধ্বশ্বাসে ধাবমান অশ্ব সমূহ) সূরা-১০০, মাক্কী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "وَالْعَادِيَاتِ ضَبْحًا (1) فَالْمُورِيَاتِ قَدْحًا (2) فَالْمُغِيرَاتِ صُبْحًا (3) فَأَثَرْنَ بِهِ نَقْعًا (4) فَوَسَطْنَ بِهِ جَمْعًا (5) إِنَّ الْإِنْسَانَ لِرَبِّهِ لَكَنُودٌ (6) وَإِنَّهُ عَلَى ذَلِكَ لَشَهِيدٌ (7) وَإِنَّهُ لِحُبِّ الْخَيْرِ لَشَدِيدٌ (8) أَفَلَا يَعْلَمُ إِذَا بُعْثِرَ مَا فِي الْقُبُورِ (9) وَحُصِّلَ مَا فِي الصُّدُورِ (10) إِنَّ رَبَّهُمْ بِهِمْ يَوْمَئِذٍ لَخَبِيرٌ (11)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = "(১) ওয়াল ‘আ-দিইয়া-তে যাবহান (২) ফালমূরিয়া-তে ক্বাদহান (৩) ফালমুগীরা-তে ছুবহা (৪) ফাআছারনা বিহী নাক্ব‘আন (৫) ফাওয়াসাত্বনা বিহী জাম‘আ (৬) ইন্নাল ইনসা-না লেরবিবহি লাকানূদ (৭) ওয়া ইন্নাহূ ‘আলা যা-লিকা লাশাহীদ (৮) ওয়া ইন্নাহূ লেহুবিবল খায়রে লাশাদীদ (৯) আফালা ইয়া‘লামু এযা বু‘ছিরা মা ফিল ক্বুবূর (১০) ওয়া হুছছিলা মা ফিছ ছুদূর (১১) ইন্না রববাহুম বিহিম ইয়াওমাইযিল লাখাবীর।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "(১) শপথ ঊর্ধ্বশ্বাসে ধাবমান অশ্ব সমূহের। (২) অতঃপর ক্ষুরাঘাতে অগ্নি বিচ্ছুরক অশ্বসমূহের। (৩) অতঃপর প্রভাতকালে আক্রমণকারী অশ্ব সমূহের (৪) যারা সে সময় ধূলি উৎক্ষেপন করে। (৫) অতঃপর যারা শত্রুদলের অভ্যন্তরে ঢুকে পড়ে। (৬) নিশ্চয়ই মানুষ তার পালনকর্তার প্রতি অকৃতজ্ঞ। (৭) আর সে নিজেই (তার কর্মের দ্বারা) এ বিষয়ে সাক্ষী। (৮) নিশ্চয়ই সে ধন-সম্পদের মায়ায় অন্ধ। (৯) সে কি জানেনা, যখন উত্থিত হবে কবরে যা কিছু আছে? (অর্থাৎ সকল মানুষ পুনরুত্থিত হবে) (১০) এবং সবকিছু প্রকাশিত হবে, যা লুকানো ছিল বুকের মধ্যে। (১১) নিশ্চয়ই তাদের প্রতিপালক সেদিন (অর্থাৎ ক্বিয়ামতের দিন) তাদের কি হবে, সে বিষয়ে সম্যক অবগত।";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;

                    case 2:


                        head = "সূরা ক্বা-রে‘আহ (করাঘাতকারী) সূরা-১০১, মাক্কী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "الْقَارِعَةُ (1) مَا الْقَارِعَةُ (2) وَمَا أَدْرَاكَ مَا الْقَارِعَةُ (3) يَوْمَ يَكُونُ النَّاسُ كَالْفَرَاشِ الْمَبْثُوثِ (4) وَتَكُونُ الْجِبَالُ كَالْعِهْنِ الْمَنْفُوشِ (5) فَأَمَّا مَنْ ثَقُلَتْ مَوَازِينُهُ (6) فَهُوَ فِي عِيشَةٍ رَاضِيَةٍ (7) وَأَمَّا مَنْ خَفَّتْ مَوَازِينُهُ (8) فَأُمُّهُ هَاوِيَةٌ (9) وَمَا أَدْرَاكَ مَا هِيَهْ (10) نَارٌ حَامِيَةٌ (11)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = "(১) আলক্বা-রে‘আতু (২) মাল ক্বা-রে‘আহ (৩) ওয়া মা আদরা-কা মাল ক্বা-রে‘আহ (৪) ইয়াওমা ইয়াকূনুন না-সু কাল ফারা-শিল মাবছূছ (৫) ওয়া তাকূনুল জিবা-লু কাল ‘ইহ্নিল মানফূশ (৬) ফাআম্মা মান ছাক্বুলাত মাওয়া-ঝীনুহু (৭) ফাহুয়া ফী ‘ঈশাতির রা-যিয়াহ (৮) ওয়া আম্মা মান খাফফাত মাওয়া-ঝীনুহু (৯) ফাউম্মুহূ হা-ভিয়াহ (১০) ওয়া মা আদরা-কা মা হিয়াহ (১১) না-রুন হা-মিয়াহ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "(১) করাঘাতকারী! (২) করাঘাতকারী কি? (৩) আপনি কি জানেন, করাঘাতকারী কি? (৪) যেদিন মানুষ হবে বিক্ষিপ্ত পতঙ্গের মত (৫) এবং পর্বতমালা হবে ধুনিত রঙিন পশমের মত। (৬) অতঃপর যার (সৎকর্মের) ওযনের পাল্লা ভারি হবে, (৭) সে (জান্নাতে) সুখী জীবন যাপন করবে। (৮) আর যার (সৎকর্মের) ওযনের পাল্লা হালকা হবে, (৯) তার ঠিকানা হবে ‘হাভিয়াহ’। (১০) আপনি কি জানেন তা কি? (১১) প্রজ্জ্বলিত অগ্নি।";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;
                    case 3:


                        head = "সূরা তাকাছুর (অধিক পাওয়ার আকাংখা) সূরা-১০২, মাক্কী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "أَلْهَاكُمُ التَّكَاثُرُ (1) حَتَّى زُرْتُمُ الْمَقَابِرَ (2) كَلَّا سَوْفَ تَعْلَمُونَ (3) ثُمَّ كَلَّا سَوْفَ تَعْلَمُونَ (4) كَلَّا لَوْ تَعْلَمُونَ عِلْمَ الْيَقِينِ (5) لَتَرَوُنَّ الْجَحِيمَ (6) ثُمَّ لَتَرَوُنَّهَا عَيْنَ الْيَقِينِ (7) ثُمَّ لَتُسْأَلُنَّ يَوْمَئِذٍ عَنِ النَّعِيمِ (8)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = " (১) আলহা-কুমুত তাকা-ছুর (২) হাত্তা ঝুরতুমুল মাক্বা-বির (৩) কাল্লা সাওফা তা‘লামূনা (৪) ছুম্মা কাল্লা সাওফা তা‘লামূন (৫) কাল্লা লাও তা‘লামূনা ‘ইলমাল ইয়াক্বীন (৬) লাতারাভুন্নাল জাহীম (৭) ছুম্মা লাতারাভুন্নাহা ‘আয়নাল ইয়াক্বীন (৮) ছুম্মা লাতুসআলুন্না ইয়াওমাইযিন ‘আনিন না‘ঈম।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "(১) অধিক পাওয়ার আকাংখা তোমাদের (পরকাল থেকে) গাফেল রাখে, (২) যতক্ষণ না তোমরা কবরে উপনীত হও। (৩) কখনই না। শীঘ্র তোমরা জানতে পারবে। (৪) অতঃপর কখনই না। শীঘ্র তোমরা জানতে পারবে (৫) কখনই না। যদি তোমরা নিশ্চিত জ্ঞান রাখতে (তাহ’লে কখনো তোমরা পরকাল থেকে গাফেল হ’তে না)। (৬) তোমরা অবশ্যই জাহান্নাম প্রত্যক্ষ করবে। (৭) অতঃপর তোমরা অবশ্যই তা দিব্য-প্রত্যয়ে দেখবে। (৮) অতঃপর তোমরা অবশ্যই সেদিন তোমাদের দেওয়া নে‘মতরাজি সম্পর্কে জিজ্ঞাসিত হবে।";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;

                    case 4:


                        head = "সূরা আছর (কাল) সূরা-১০৩, মাক্কী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "وَالْعَصْرِ (1) إِنَّ الْإِنْسَانَ لَفِي خُسْرٍ (2)إِلَّا الَّذِينَ آمَنُوا وَعَمِلُوا الصَّالِحَاتِ وَتَوَاصَوْا بِالْحَقِّ وَتَوَاصَوْا بِالصَّبْرِ (3)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = " (১) ওয়াল ‘আছর (২) ইন্নাল ইনসা-না লাফী খুস্র (৩) ইল্লাল্লাযীনা আ-মানু ওয়া ‘আমিলুছ ছা-লেহা-তে, ওয়া তাওয়া-ছাও বিল হাকক্বে ওয়া তাওয়া-ছাও বিছ্ ছাব্র ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "(১) কালের শপথ! (২) নিশ্চয়ই সকল মানুষ অবশ্যই ক্ষতির মধ্যে রয়েছে। (৩) তারা ব্যতীত যারা (জেনে-বুঝে) ঈমান এনেছে ও সৎকর্ম সম্পাদন করেছে এবং পরস্পরকে ‘হক’-এর উপদেশ দিয়েছে ও পরস্পরকে ধৈর্য্যের উপদেশ দিয়েছে।";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;

                    case 5:


                        head = "সূরা হুমাযাহ (নিন্দাকারী) সূরা-১০৪, মাক্কী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "وَيْلٌ لِكُلِّ هُمَزَةٍ لُمَزَةٍ (1) الَّذِي جَمَعَ مَالًا وَعَدَّدَهُ (2) يَحْسَبُ أَنَّ مَالَهُ أَخْلَدَهُ (3) كَلَّا لَيُنْبَذَنَّ فِي الْحُطَمَةِ (4) وَمَا أَدْرَاكَ مَا الْحُطَمَةُ (5) نَارُ اللَّهِ الْمُوقَدَةُ (6) الَّتِي تَطَّلِعُ عَلَى الْأَفْئِدَةِ (7) إِنَّهَا عَلَيْهِمْ مُؤْصَدَةٌ (8) فِي عَمَدٍ مُمَدَّدَةٍ (9)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = " (১) ওয়ায়লুল লেকুল্লে হুমাঝাতিল লুমাঝাহ (২) আল্লাযী জামা‘আ মা-লাওঁ ওয়া ‘আদ্দাদাহ (৩) ইয়াহ্সাবু আন্না মা-লাহূ আখলাদাহ (৪) কাল্লা লাইয়ুম্বাযান্না ফিল হুত্বামাহ (৫) ওয়া মা আদরা-কা মাল হুত্বামাহ্? (৬) না-রুল্লা-হিল মূক্বাদাহ (৭) আল্লাতী তাত্ত্বালি‘উ ‘আলাল আফ্ইদাহ (৮) ইন্নাহা ‘আলাইহিম মু’ছাদাহ (৯) ফী ‘আমাদিম মুমাদ্দাদাহ ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "(১) দুর্ভোগ সেই সব ব্যক্তির জন্য যারা পশ্চাতে নিন্দা করে ও সম্মুখে নিন্দা করে (২) এবং সম্পদ জমা করে ও গণনা করে (৩) সে ধারণা করে যে, তার মাল তাকে চিরস্থায়ী করে রাখবে (৪) কখনোই না। সে অবশ্য অবশ্যই নিক্ষিপ্ত হবে পিষ্টকারী হুত্বামাহর মধ্যে (৫) আপনি কি জানেন ‘হুত্বামাহ’ কি? (৬) এটা আল্লাহর প্রজ্জ্বলিত অগ্নি (৭) যা কলিজা পর্যন্ত পৌঁছে যাবে (৮) এটা তাদের উপরে পরিবেষ্টিত থাকবে (৯) দীর্ঘ স্তম্ভ সমূহে।";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;
                    case 6:


                        head = "সূরা ফীল (হাতি) সূরা-১০৫, মাক্কী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "أَلَمْ تَرَ كَيْفَ فَعَلَ رَبُّكَ بِأَصْحَابِ الْفِيلِ (1) أَلَمْ يَجْعَلْ كَيْدَهُمْ فِي تَضْلِيلٍ (2) وَأَرْسَلَ عَلَيْهِمْ طَيْرًا أَبَابِيلَ (3) تَرْمِيهِمْ بِحِجَارَةٍ مِنْ سِجِّيلٍ (4) فَجَعَلَهُمْ كَعَصْفٍ مَأْكُولٍ (5)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = " (১) আলাম তারা কায়ফা ফা‘আলা রাববুকা বে আছহা-বিল ফীল (২) আলাম ইয়াজ্‘আল কায়দাহুম ফী তাযলীল? (৩) ওয়া আরসালা ‘আলাইহিম ত্বায়রান আবা-বীল (৪) তারমীহিম বি হিজা-রাতিম মিন সিজ্জীল (৫) ফাজা‘আলাহুম কা‘আছফিম মা’কূল।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "(১) আপনি কি শোনেন নি, আপনার প্রভু হস্তীওয়ালাদের সাথে কিরূপ আচরণ করেছিলেন? (২) তিনি কি তাদের চক্রান্ত নস্যাৎ করে দেননি? (৩) তিনি তাদের উপরে প্রেরণ করেছিলেন ঝাঁকে ঝাঁকে পাখি (৪) যারা তাদের উপরে নিক্ষেপ করেছিল মেটেল পাথরের কংকর (৫) অতঃপর তিনি তাদের করে দেন ভক্ষিত তৃণসদৃশ।";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;
                    case 7:


                        head = "সূরা কুরায়েশ (কুরায়েশ বংশ, কা‘বার তত্ত্বাবধায়কগণ) সূরা-১০৬, মাক্কী";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "لِإِيلَافِ قُرَيْشٍ (1) إِيلَافِهِمْ رِحْلَةَ الشِّتَاءِ وَالصَّيْفِ (2) فَلْيَعْبُدُوا رَبَّ هَذَا الْبَيْتِ (3) الَّذِي أَطْعَمَهُمْ مِنْ جُوعٍ وَآمَنَهُمْ مِنْ خَوْفٍ (4)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = " (১) লেঈলা-ফে কুরায়েশ (২) ঈলা-ফিহিম রিহলাতাশ শিতা-ই ওয়াছ ছায়েফ (৩) ফাল ইয়া‘বুদূ রববা হা-যাল বায়েত (৪) আল্লাযী আত্ব‘আমাহুম মিন জূ‘; ওয়া আ-মানাহুম মিন খাওফ ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "অনুবাদ :";
                        str11 = "(১) কুরায়েশদের আসক্তির কারণে (২) আসক্তির কারণে তাদের শীত ও গ্রীষ্মকালীন সফরের (৩) অতএব তারা যেন ইবাদত করে এই গৃহের মালিকের (৪) যিনি তাদেরকে ক্ষুধায় অন্ন দান করেছেন এবং ভীতি হ’তে নিরাপদ করেছেন।";
                        str12 = "[শীতকালে ইয়ামনে ও গ্রীষ্মকালে সিরিয়ায় ব্যবসায়িক সফরের উপরেই কুরায়েশদের জীবিকা নির্ভর করত। বায়তুল্লাহর খাদেম হওয়ার কারণে সারা আরবে তারা সম্মানিত ছিল। সেকারণ তাদের কাফেলা সর্বদা নিরাপদ থাকত।]";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;
                    case 8:

                        head = "সূরা মা-‘ঊন (নিত্য ব্যবহার্য বস্ত্ত) সূরা-১০৭, মাক্কী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "أَرَأَيْتَ الَّذِي يُكَذِّبُ بِالدِّينِ (1) فَذَلِكَ الَّذِي يَدُعُّ الْيَتِيمَ (2) وَلَا يَحُضُّ عَلَى طَعَامِ الْمِسْكِينِ (3) فَوَيْلٌ لِلْمُصَلِّينَ (4) الَّذِينَ هُمْ عَنْ صَلَاتِهِمْ سَاهُونَ (5) الَّذِينَ هُمْ يُرَاءُونَ (6) وَيَمْنَعُونَ الْمَاعُونَ (7)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = "(১) আরাআয়তাল্লাযী ইয়ুকায্যিবু বিদ্দীন? (২) ফাযা-লিকাল্লাযী ইয়াদু‘উল ইয়াতীম (৩) ওয়া লা ইয়াহুয্যু ‘আলা ত্বা-‘আ-মিল মিসকীন (৪) ফাওয়ায়লুল লিল মুছাল্লীন (৫) আল্লাযীনা হুম ‘আন ছালা-তিহিম সা-হূন (৬) আল্লাযীনা হুম ইয়ুরা-ঊনা, (৭) ওয়া ইয়ামনা‘ঊনাল মা-‘ঊন ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "(১) আপনি কি দেখেছেন তাকে, যে বিচার দিবসকে মিথ্যা বলে? (২) সে হ’ল ঐ ব্যক্তি, যে ইয়াতীমকে গলা ধাক্কা দেয় (৩) এবং মিসকীনকে খাদ্য দানে উৎসাহিত করে না (৪) অতঃপর দুর্ভোগ ঐ সব মুছল্লীর জন্য (৫) যারা তাদের ছালাত থেকে উদাসীন (৬) যারা লোকদেরকে দেখায় (৭) এবং নিত্য ব্যবহার্য বস্ত্ত দানে বিরত থাকে।";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;
                    case 9:


                        head = "সূরা কাওছার (হাউয কাওছার-জান্নাতী জলাধার) সূরা-১০৮, মাদানী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "إِنَّا أَعْطَيْنَاكَ الْكَوْثَرَ	 (1)"
                                + "فَصَلِّ لِرَبِّكَ وَانْحَرْ	 (2)"
                                + "إِنَّ شَانِئَكَ هُوَ الْأَبْتَرُ	 (3)";

                        str4 = "";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = "(১) ইন্না আ‘ত্বায়না-কাল কাওছার (২) ফাছাল্লে লে রবিবকা ওয়ান্হার (৩) ইন্না শা-নিআকা হুওয়াল আবতার ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "(১) নিশ্চয়ই আমরা আপনাকে ‘কাওছার’ দান করেছি (২) অতএব আপনার প্রভুর উদ্দেশ্যে ছালাত আদায় করুন ও কুরবানী করুন (৩) নিশ্চয়ই আপনার শত্রুই নির্বংশ।";
                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;
                    case 10:


                        head = "সূরা কা-ফিরূণ (ইসলামে অবিশ্বাসীগণ) সূরা-১০৯, মাক্কী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "قُلْ يَا أَيُّهَا الْكَافِرُونَ (1) لَا أَعْبُدُ مَا تَعْبُدُونَ (2) وَلَا أَنْتُمْ عَابِدُونَ مَا أَعْبُدُ (3) وَلَا أَنَا عَابِدٌ مَا عَبَدْتُمْ (4) وَلَا أَنْتُمْ عَابِدُونَ مَا أَعْبُدُ (5) لَكُمْ دِينُكُمْ وَلِيَ دِينِ (6)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = "(১) ক্বুল ইয়া আইয়ুহাল কা-ফিরূণ! (২) লা আ‘বুদু মা তা‘বুদূন (৩) ওয়া লা আনতুম ‘আ-বিদূনা মা আ‘বুদ (৪) ওয়া লা আনা ‘আ-বিদুম মা ‘আবাদতুম (৫) ওয়া লা আনতুম ‘আ-বিদূনা মা আ‘বুদ (৬) লাকুম দীনুকুম ওয়া লিয়া দীন ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "(১) আপনি বলুন! হে কাফেরবৃন্দ! (২) আমি ইবাদত করি না তোমরা যাদের ইবাদত কর (৩) এবং তোমরা ইবাদতকারী নও আমি যার ইবাদত করি (৪) আমি ইবাদতকারী নই তোমরা যার ইবাদত কর (৫) এবং তোমরা ইবাদতকারী নও আমি যার ইবাদত করি (৬) তোমাদের জন্য তোমাদের দ্বীন এবং আমার জন্য আমার দ্বীন।";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;
                    case 11:

                        head = "সূরা নছর (সাহায্য) সূরা-১১০, মাদানী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "إِذَا جَاءَ نَصْرُ اللَّهِ وَالْفَتْحُ (1) وَرَأَيْتَ النَّاسَ يَدْخُلُونَ فِي دِينِ اللَّهِ أَفْوَاجًا (2) فَسَبِّحْ بِحَمْدِ رَبِّكَ وَاسْتَغْفِرْهُ إِنَّهُ كَانَ تَوَّابًا (3)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = " (১) ইযা জা-আ নাছরুল্লা-হি ওয়াল ফাৎহু (২) ওয়া রাআয়তান্না-সা ইয়াদখুলূনা ফী দী-নিল্লা-হি আফওয়া-জা (৩) ফাসাবিবহ বিহাম্দি রবিবকা ওয়াস্তাগফির্হু, ইন্নাহূ কা-না তাউওয়া-বা ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "(১) যখন এসে গেছে আল্লাহর সাহায্য ও (মক্কা) বিজয় (২) এবং আপনি মানুষকে দেখছেন দলে দলে আল্লাহর দ্বীনে (ইসলামে) প্রবেশ করছে (৩) তখন আপনি আপনার পালনকর্তার প্রশংসা সহ পবিত্রতা বর্ণনা করুন এবং তাঁর নিকটে ক্ষমা প্রার্থনা করুন। নিশ্চয়ই তিনি অধিক তওবা কবুলকারী।";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;

                    case 12:


                        head = "সূরা লাহাব (অগ্নি স্ফূলিঙ্গ) সূরা-১১১, মাক্কী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "تَبَّتْ يَدَا أَبِي لَهَبٍ وَتَبَّ (1) مَا أَغْنَى عَنْهُ مَالُهُ وَمَا كَسَبَ (2) سَيَصْلَى نَارًا ذَاتَ لَهَبٍ (3) وَامْرَأَتُهُ حَمَّالَةَ الْحَطَبِ (4) فِي جِيدِهَا حَبْلٌ مِنْ مَسَدٍ (5)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = " (১) তাববাত ইয়াদা আবী লাহাবিউঁ ওয়া তাববা (২) মা আগনা ‘আন্হু মা-লুহূ ওয়া মা কাসাব (৩) সাইয়াছলা না-রাণ যা-তা লাহাবিউঁ (৪) ওয়ামরাআতুহূ, হাম্মা-লাতাল হাত্বাব (৫) ফী জীদিহা হাবলুম মিম মাসাদ ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "অনুবাদ :";
                        str11 = "(১) আবু লাহাবের হস্তদ্বয় ধ্বংস হৌক এবং ধ্বংস হৌক সে নিজে (২) তার কোন কাজে আসেনি তার ধন-সম্পদ ও যা কিছু সে উপার্জন করেছে (৩) সত্বর সে প্রবেশ করবে লেলিহান অগ্নিতে (৪) এবং তার স্ত্রীও; যে ইন্ধন বহনকারিণী (৫) তার গলদেশে খর্জুর পত্রের পাকানো রশি।";
                        str12 = "[আবু লাহাব ছিল রাসূল (ছাঃ)-এর চাচা ও নিকটতম শত্রু প্রতিবেশী। তার স্ত্রী ছিল আবু সুফিয়ানের বোন উম্মে জামীল।]";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();


                        break;
                    case 13:

                        head = "সূরা ইখলাছ (খালেছ বিশ্বাস) সূরা-১১২, মাক্কী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "قُلْ هُوَ اللَّهُ أَحَدٌ	 (1)" + "اللَّهُ الصَّمَدُ	 (2)"
                                + "لَمْ يَلِدْ وَلَمْ يُولَدْ	 (3)"
                                + "وَلَمْ يَكُن لَّهُ كُفُوًا أَحَدٌ	 (4)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = "(১) ক্বুল হুওয়াল্লা-হু আহাদ (২) আল্লা-হুছ ছামাদ (৩) লাম ইয়ালিদ ওয়া লাম ইয়ুলাদ (৪) ওয়া লাম ইয়াকুল্লাহূ কুফুওয়ান আহাদ ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "(১) বলুন, তিনি আল্লাহ এক (২) আল্লাহ মুখাপেক্ষীহীন (৩) তিনি (কাউকে) জন্ম দেননি এবং তিনি (কারও) জন্মিত নন (৪) এবং তাঁর সমতুল্য কেউ নেই।";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;

                    case 14:
                        head = "সূরা ফালাক্ব (প্রভাতকাল) সূরা-১১৩, মাদানী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "قُلْ أَعُوذُ بِرَبِّ الْفَلَقِ (1) مِنْ شَرِّ مَا خَلَقَ (2) وَمِنْ شَرِّ غَاسِقٍ إِذَا وَقَبَ (3) وَمِنْ شَرِّ النَّفَّاثَاتِ فِي الْعُقَدِ (4) وَمِنْ شَرِّ حَاسِدٍ إِذَا حَسَدَ (5)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = "(১) ক্বুল আ‘ঊযু বি রবিবল ফালাক্ব (২) মিন শার্রি মা খালাক্ব (৩) ওয়া মিন শার্রি গা-সিক্বিন ইযা ওয়াক্বাব (৪) ওয়া মিন শার্রিন নাফ্ফা-ছা-তি ফিল ‘উক্বাদ (৫) ওয়া মিন শার্রি হা-সিদিন ইযা হাসাদ ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "(১) বলুন! আমি আশ্রয় গ্রহণ করছি প্রভাতের প্রতিপালকের (২) যাবতীয় অনিষ্ট হ’তে, যা তিনি সৃষ্টি করেছেন (৩) এবং অন্ধকার রাত্রির অনিষ্ট হ’তে, যখন তা আচছন্ন হয় (৪) গ্রন্থিতে ফুঁকদান কারিণীদের অনিষ্ট হ’তে (৫) এবং হিংসুকের অনিষ্ট হ’তে যখন সে হিংসা করে।";
                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;
                    case 15:


                        head = "সূরা নাস (মানব জাতি) সূরা-১১৪, মাদানী ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "قُلْ أَعُوذُ بِرَبِّ النَّاسِ (1) مَلِكِ النَّاسِ (2) إِلَهِ النَّاسِ (3) مِنْ شَرِّ الْوَسْوَاسِ الْخَنَّاسِ (4) الَّذِي يُوَسْوِسُ فِي صُدُورِ النَّاسِ (5) مِنَ الْجِنَّةِ وَالنَّاسِ (6)";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = " (১) ক্বুল আ‘ঊযু বি রবিবন্না-স (২) মালিকিন্না-স (৩) ইলা-হিন্না-স (৪) মিন শার্রিল ওয়াস্ওয়া-সিল খান্না-স (৫) আল্লাযী ইয়ুওয়াস্ভিসু ফী ছুদূরিন্না-স (৬) মিনাল জিন্নাতি ওয়ান্না-স।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "(১) বলুন! আমি আশ্রয় গ্রহণ করছি মানুষের পালনকর্তার (২) মানুষের অধিপতির (৩) মানুষের উপাস্যের (৪) গোপন কুমন্ত্রণাদাতার অনিষ্ট হ’তে (৫) যে কুমন্ত্রণা দেয় মানুষের অন্তর সমূহে (৬) জিনের মধ্য হ’তে ও মানুষের মধ্য হ’তে।";


                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;

                    case 16:


                        head = "তাকবীরে তাহরীমা ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "اَللَّهُمَّ بَاعِدْ بَيْنِيْ وَبَيْنَ خَطَايَايَ كَمَا بَاعَدْتَ بَيْنَ الْمَشْرِقِ وَالْمَغْرِبِ اَللَّهُمَّ نَقِّنِيْ مِنَ الْخَطَايَا كَمَا يُنَقَّى الثَّوْبُ الْأَبْيَضُ مِنَ الدَّنَسِ، اَللَّهُمَّ اغْسِلْ خَطَايَايَ بِالْمَاءِ وَالثَّلْجِ وَالْبَرَد-";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = " আল্লা-হুম্মা বা-‘এদ বায়নী ওয়া বায়না খাত্বা-ইয়া-ইয়া, কামা বা-‘আদতা বায়নাল মাশরিক্বি ওয়াল মাগরিবি। আল্লা-হুম্মা নাকক্বিনী মিনাল খাত্বা-ইয়া, কামা ইউনাকক্বাছ ছাওবুল আব্ইয়াযু মিনাদ দানাসি। আল্লা-হুম্মাগ্সিল খাত্বা-ইয়া-ইয়া বিল মা-য়ি ওয়াছ ছালজি ওয়াল বারাদি’।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "হে আল্লাহ! আপনি আমার ও আমার গোনাহ সমূহের মধ্যে এমন দূরত্ব সৃষ্টি করে দিন, যেমন দূরত্ব সৃষ্টি করেছেন পূর্ব ও পশ্চিমের মধ্যে। হে আল্লাহ! আপনি আমাকে পরিচ্ছন্ন করুন গোনাহ সমূহ হ’তে, যেমন পরিচ্ছন্ন করা হয় সাদা কাপড় ময়লা হ’তে। হে আল্লাহ! আপনি আমার গুনাহ সমূহকে ধুয়ে ছাফ করে দিন পানি দ্বারা, বরফ দ্বারা ও শিশির দ্বারা’।[মুত্তাফাক্ব ‘আলাইহ, মিশকাত হা/৮১২ ‘তাকবীরের পর যা পড়তে হয়’ অনুচ্ছেদ-১১।]একে ‘ছানা’ বা দো‘আয়ে ইস্তেফতাহ বলা হয়। ছানার জন্য অন্য দো‘আও রয়েছে। তবে এই দো‘আটি সর্বাধিক বিশুদ্ধ।";


                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;

                    case 17:


                        head = "দুই সিজদার মধ্যবর্তী বৈঠকের দো‘আ ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "اَللَّهُمَّ اغْفِرْ لِيْ وَارْحَمْنِيْ وَاجْبُرْنِيْ وَاهْدِنِيْ وَعَافِنِيْ وَارْزُقْنِيْ-";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = "আল্লা-হুম্মাগ্ফিরলী ওয়ারহাম্নী ওয়াজ্বুরনী ওয়াহ্দিনী ওয়া ‘আ-ফেনী ওয়ার্ঝুক্বনী ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = " ‘হে আল্লাহ! আপনি আমাকে ক্ষমা করুন, আমার উপরে রহম করুন, আমার অবস্থার সংশোধন করুন, আমাকে সৎপথ প্রদর্শন করুন, আমাকে সুস্থতা দান করুন ও আমাকে রূযী দান করুন’।[তিরমিযী হা/২৮৪; ইবনু মাজাহ হা/৮৯৮; আবুদাঊদ হা/৮৫০; ঐ, মিশকাত হা/৯০০, অনুচ্ছেদ-১৪; নায়লুল আওত্বার ৩/১২৯ পৃঃ।]";


                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;

                    case 18:


                        head = "তাশাহ্হুদ (আত্তাহিইয়া-তু)";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "اَلتَّحِيَّاتُ ِللهِ وَالصَّلَوَاتُ وَالطَّيِّبَاتُ، السَّلاَمُ عَلَيْكَ أَيُّهَا النَّبِيُّ وَرَحْمَةُ اللهِ وَبَرَكَاتُهُ، السَّلاَمُ عَلَيْنَا وَعَلَى عِبَادِ اللهِ الصَّالِحِيْنَ، أَشْهَدُ أَنْ لاَّ إِلَهَ إِلاَّ اللهُ وَأَشْهَدُ أَنَّ مُحَمَّدًا عَبْدُهُ وَرَسُوْلُهُ-";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = "আত্তাহিইয়া-তু লিল্লা-হি ওয়াছ্ ছালাওয়া-তু ওয়াত্ ত্বাইয়িবা-তু আসসালা-মু ‘আলায়কা আইয়ুহান নাবিইয়ু ওয়া রহমাতুল্লা-হি ওয়া বারাকা-তুহু। আসসালা-মু ‘আলায়না ওয়া ‘আলা ‘ইবা-দিল্লা-হিছ ছা-লেহীন। আশহাদু আল লা-ইলা-হা ইল্লাল্লা-হু ওয়া আশহাদু আনণা মুহাম্মাদান ‘আব্দুহূ ওয়া রাসূলুহু ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = "যাবতীয় সম্মান, যাবতীয় উপাসনা ও যাবতীয় পবিত্র বিষয় আল্লাহর জন্য। হে নবী! আপনার উপরে শান্তি বর্ষিত হৌক এবং আল্লাহর অনুগ্রহ ও সমৃদ্ধি সমূহ নাযিল হউক। শান্তি বর্ষিত হউক আমাদের উপরে ও আল্লাহর সৎকর্মশীল বান্দাগণের উপরে। আমি সাক্ষ্য দিচ্ছি যে, আল্লাহ ব্যতীত কোন উপাস্য নেই এবং আমি সাক্ষ্য দিচ্ছি যে, মুহাম্মাদ তাঁর বান্দা ও রাসূল’ (বুঃ মুঃ)।[মুত্তাফাক্ব ‘আলাইহ, মিশকাত হা/৯০৯ ‘ছালাত’ অধ্যায়-৪, ‘তাশাহহুদ’ অনুচ্ছেদ-১৫।]";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;
                    case 19:


                        head = "দরূদ ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "اَللَّهُمَّ صَلِّ عَلَى مُحَمَّدٍ وَّعَلَى آلِ مُحَمَّدٍ كَمَا صَلَّيْتَ عَلَى إِبْرَاهِيْمَ وَعَلَى آلِ إِبْرَاهِيْمَ إِنَّكَ حَمِيْدٌ مَّجِيْدٌ- اَللَّهُمَّ بَارِكْ عَلَى مُحَمَّدٍ وَّعَلَى آلِ مُحَمَّدٍ كَمَا بَارَكْتَ عَلَى إِبْرَاهِيْمَ وَعَلَى آلِ إِبْرَاهِيْمَ إِنَّكَ حَمِيْدٌ مَّجِيْدٌ-";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = "আল্লা-হুম্মা ছাল্লে ‘আলা মুহাম্মাদিঁউ ওয়া ‘আলা আ-লে মুহাম্মাদিন কামা ছাল্লায়তা ‘আলা ইবরা-হীমা ওয়া ‘আলা আ-লে ইব্রা-হীমা ইন্নাকা হামীদুম মাজীদ। আল্লা-হুম্মা বা-রিক ‘আলা মুহাম্মাদিঁউ ওয়া ‘আলা আ-লে মুহাম্মাদিন কামা বা-রক্তা ‘আলা ইব্রা-হীমা ওয়া ‘আলা আ-লে ইব্রা-হীমা ইন্নাকা হামীদুম মাজীদ ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = " ‘হে আল্লাহ! আপনি রহমত বর্ষণ করুন মুহাম্মাদ ও মুহাম্মাদের পরিবারের উপরে, যেমন আপনি রহমত বর্ষণ করেছেন ইবরাহীম ও ইবরাহীমের পরিবারের উপরে। নিশ্চয়ই আপনি প্রশংসিত ও সম্মানিত। হে আল্লাহ! আপনি বরকত নাযিল করুন মুহাম্মাদ ও মুহাম্মাদের পরিবারের উপরে, যেমন আপনি বরকত নাযিল করেছেন ইবরাহীম ও ইবরাহীমের পরিবারের উপরে। নিশ্চয়ই আপনি প্রশংসিত ও সম্মানিত’।[মুত্তাফাক্ব ‘আলাইহ, মিশকাত হা/৯১৯ ‘রাসূল (ছাঃ)-এর উপর দরূদ পাঠ’ অনুচ্ছেদ-১৬; ছিফাত ১৪৭ পৃঃ, টীকা ২-৩ দ্রঃ।]";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;
                    case 20:


                        head = "দো‘আয়ে মাছূরাহ ";
                        str1 = "بِسْمِ اللہِ الرَّحْمٰنِ الرَّحِیْمِ";
                        str2 = "";
                        str3 = "";
                        str4 = "اَللَّهُمَّ إِنِّيْ ظَلَمْتُ نَفْسِيْ ظُلْمًا كَثِيْرًا وَّلاَ يَغْفِرُ الذُّنُوْبَ إِلاَّ أَنْتَ، فَاغْفِرْ لِيْ مَغْفِرَةً مِّنْ عِنْدَكَ وَارْحَمْنِيْ إِنَّكَ أَنْتَ الْغَفُوْرُ الرَّحِيْمُ-";
                        str5 = "";
                        str6 = "উচ্চারণ : ";
                        str7 = "আল্লা-হুম্মা ইন্নী যালামতু নাফ্সী যুলমান কাছীরাঁও অলা ইয়াগ্ফিরুয যুনূবা ইল্লা আন্তা, ফাগ্ফিরলী মাগফিরাতাম মিন ‘ইনদিকা ওয়ারহাম্নী ইন্নাকা আন্তাল গাফূরুর রহীম’ ।";
                        str8 = "";
                        str9 = "পরম করুণাময় অসীম দয়ালু আল্লাহর নামে (শুরু করছি)";
                        str10 = "";
                        str11 = "অনুবাদ :";
                        str12 = " ‘হে আল্লাহ! আমি আমার নফসের উপরে অসংখ্য যুলুম করেছি। ঐসব গুনাহ মাফ করার কেউ নেই আপনি ব্যতীত। অতএব আপনি আমাকে আপনার পক্ষ হ’তে বিশেষভাবে ক্ষমা করুন এবং আমার উপরে অনুগ্রহ করুন। নিশ্চয়ই আপনি ক্ষমাশীল ও দয়াবান’।[ মুত্তাফাক্ব ‘আলাইহ, মিশকাত হা/৯৪২ ‘তাশাহহুদে দো‘আ’ অনুচ্ছেদ-১৭; বুখারী হা/৮৩৪ ‘আযান’ অধ্যায়-২, ‘সালামের পূর্বে দো‘আ’ অনুচ্ছেদ-১৪৯।]";

                        intent = new Intent();
                        intent.setClass(getActivity(), DetailsActivity.class);


                        intent.putExtra("hd", head);
                        intent.putExtra("st1", str1);
                        intent.putExtra("st2", str2);
                        intent.putExtra("st3", str3);
                        intent.putExtra("st4", str4);
                        intent.putExtra("st5", str5);
                        intent.putExtra("st6", str6);
                        intent.putExtra("st7", str7);
                        intent.putExtra("st8", str8);
                        intent.putExtra("st9", str9);
                        intent.putExtra("st10", str10);
                        intent.putExtra("st11", str11);
                        intent.putExtra("st12", str12);

                        startActivity(intent);
                        getActivity().finish();

                        break;


                    default:
                        break;
                }

            }

        });

        return view;
    }

    public static class ViewHolder {
        public TextView titles;
        public TextView Description;
        public ImageView images;
    }

    public class CustomAdapter extends BaseAdapter {

        Context contexT;

        LayoutInflater inflater;
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options;
        String[] texts = {

                "(১) সূরা যিলযাল (ভূমিকম্প) সূরা-৯৯, মাক্কী ",
                "(২) সূরা ‘আদিয়াত (ঊর্ধ্বশ্বাসে ধাবমান অশ্ব সমূহ) সূরা-১০০, মাক্কী ",
                "(৩) সূরা ক্বা-রে‘আহ (করাঘাতকারী) সূরা-১০১, মাক্কী ",
                "(৪) সূরা তাকাছুর (অধিক পাওয়ার আকাংখা) সূরা-১০২, মাক্কী ",
                "(৫) সূরা আছর (কাল) সূরা-১০৩, মাক্কী ",
                "(৬) সূরা হুমাযাহ (নিন্দাকারী) সূরা-১০৪, মাক্কী ",
                "(৭) সূরা ফীল (হাতি) সূরা-১০৫, মাক্কী ",
                "(৮) সূরা কুরায়েশ (কুরায়েশ বংশ, কা‘বার তত্ত্বাবধায়কগণ) সূরা-১০৬, মাক্কী",
                "(৯) সূরা মা-‘ঊন (নিত্য ব্যবহার্য বস্ত্ত) সূরা-১০৭, মাক্কী ",
                "(১০) সূরা কাওছার (হাউয কাওছার-জান্নাতী জলাধার) সূরা-১০৮, মাদানী ",
                "(১১) সূরা কা-ফিরূণ (ইসলামে অবিশ্বাসীগণ) সূরা-১০৯, মাক্কী ",
                "(১২) সূরা নছর (সাহায্য) সূরা-১১০, মাদানী ",
                "(১৩) সূরা লাহাব (অগ্নি স্ফূলিঙ্গ) সূরা-১১১, মাক্কী ",
                "(১৪) সূরা ইখলাছ (খালেছ বিশ্বাস) সূরা-১১২, মাক্কী ",
                "(১৫) সূরা ফালাক্ব (প্রভাতকাল) সূরা-১১৩, মাদানী ",
                "(১৬) সূরা নাস (মানব জাতি) সূরা-১১৪, মাদানী ",
                "(১৭) তাকবীরে তাহরীমা ",
                "(১৮) দুই সিজদার মধ্যবর্তী বৈঠকের দো‘আ ",
                "(১৯) তাশাহ্হুদ (আত্তাহিইয়া-তু)",
                "(২০) দরূদ ",
                "(২১) দো‘আয়ে মাছূরাহ ",


        };
        String[] images = {

                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",


        };

        public CustomAdapter(Context context) {
            contexT = context;

            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            Resources res = context.getResources();
            // String[] titles = res.getStringArray(R.array.titles);
            // String[] description = res.getStringArray(R.array.descriptions);
//
//            imageLoader.init(ImageLoaderConfiguration.createDefault(context));
//            options = new DisplayImageOptions.Builder()
//                    .showImageForEmptyUri(R.drawable.namaj_icon)
//                    .showImageOnFail(R.drawable.namaj_icon)
//                    .resetViewBeforeLoading(true).cacheOnDisc(true)
//                    .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
//                    .bitmapConfig(Bitmap.Config.ALPHA_8)
//                    .considerExifParams(true)
//                    .displayer(new FadeInBitmapDisplayer(50)).build();

        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return images.length;
        }

        @Override
        public Object getItem(int pos) {
            // TODO Auto-generated method stub
            return pos;
        }

        @Override
        public long getItemId(int arg0) {
            // TODO Auto-generated method stub
            return 0;
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            inflater = (LayoutInflater) contexT
                    .getSystemService(contexT.LAYOUT_INFLATER_SERVICE);
            View v;
            ViewHolder holder = null;
            if (convertView == null) {
                v = inflater.inflate(R.layout.fragment_sura, parent, false);
                holder = new ViewHolder();
                v.setTag(holder);
            } else {
                v = convertView;
                holder = (ViewHolder) v.getTag();
            }
            convertView = inflater.inflate(R.layout.fragment_sura, null);
           // ImageView IV = (ImageView) convertView.findViewById(R.id.imageView1);
            // IV.setBackground(images[pos]);
           // imageLoader.displayImage(images[pos], IV, options);
            TextView tv = (TextView) convertView.findViewById(R.id.textView1);
            tv.setText(texts[pos]);


            return convertView;
        }

    }

}
