package bytelabapps.ramadansalattime.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bytelabapps.ramadansalattime.R;

/**
 * Created by shahimtiyaj on 4/26/2016.
 */
public class RomjanKoroniyoFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.romajane_amader_koroniyo, null);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("back",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("now","koronio");
        editor.apply();

        return v;
    }


}




