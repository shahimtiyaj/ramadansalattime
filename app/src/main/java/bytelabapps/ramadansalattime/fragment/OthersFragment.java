package bytelabapps.ramadansalattime.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bytelabapps.ramadansalattime.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class OthersFragment extends Fragment {




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.fragment_others, null);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("back",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("now","others");
        editor.apply();

        return v;
    }




}




