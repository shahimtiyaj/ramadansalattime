package bytelabapps.ramadansalattime.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import bytelabapps.ramadansalattime.R;
import bytelabapps.ramadansalattime.helper.GetTime;


/**
 * A simple {@link Fragment} subclass.
 */
public class TodayFragment extends Fragment {

    InterstitialAd mInterstitialAd;

    public TodayFragment() {
        // Required empty public constructor
    }

    int sehri_hour;
    int sehri_min;
    int iftar_hour;
    int iftar_min;
    int match;
    private static final String FRAG_TAG_TIME_PICKER = "timePickerDialogFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_today, container, false);


        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId(getString(R.string.fullscreenad));
        requestNewInterstitial();

        match = 0;

        String date_format = "dd MMMM, EEEE";
        String todays_date = new SimpleDateFormat(date_format, new Locale("bn", "BD")).format(new Date());

        TextView showdate = (TextView) view.findViewById(R.id.dateToday);
        TextView sehri = (TextView) view.findViewById(R.id.sehri);
        TextView iftar = (TextView) view.findViewById(R.id.iftar);
        TextView today = (TextView) view.findViewById(R.id.today);
        CardView full_calendar = (CardView) view.findViewById(R.id.fullcal);

        GetTime getTime = new GetTime(getActivity());

        String[] dates = getResources().getStringArray(R.array.titles2);
        String[] alt_dates = getResources().getStringArray(R.array.titles3);
        String[] sehritime = (String[]) getTime.getSehri();
        String[] iftarTime = (String[]) getTime.getIftar();
        String[] romjan = getResources().getStringArray(R.array.descriptions1);

        showdate.setText(todays_date);

        for (int i=0;i<30;i++) {
            if ((dates[i].equals(todays_date))||(alt_dates[i]).equals(todays_date) ) {
                match = 1;
                sehri.setText(sehritime[i]);
                iftar.setText(iftarTime[i]);
                today.setText("আজ " + romjan[i]);


                SimpleDateFormat format = new SimpleDateFormat("hh:mm a");

                try {
                    Date sehridate = format.parse(sehritime[i]);
                    Date iftardate = format.parse(iftarTime[i]);
                    sehri_hour = sehridate.getHours();
                    sehri_min = sehridate.getMinutes();
                    iftar_hour = iftardate.getHours();
                    iftar_min = iftardate.getMinutes();
                } catch (ParseException e) {

                }

            }
        }

        full_calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                }

                CalenderFragment calenderFragment = new CalenderFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, calenderFragment).addToBackStack("calfrag");
                fragmentTransaction.commit();
            }
        });

        final ImageView sehri_alarm = (ImageView) view.findViewById(R.id.sehrifab);
        final ImageView iftar_alarm = (ImageView) view.findViewById(R.id.iftarfab);

        final Intent i = new Intent(AlarmClock.ACTION_SET_ALARM);

        sehri_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (match == 1) {
                    RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                            .setOnTimeSetListener(new RadialTimePickerDialogFragment.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
                                    i.putExtra(AlarmClock.EXTRA_MESSAGE, "সাহরীর সময় হয়েছে");
                                    i.putExtra(AlarmClock.EXTRA_HOUR, hourOfDay);
                                    i.putExtra(AlarmClock.EXTRA_MINUTES, minute);
                                    i.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
                                    startActivity(i);
                                }
                            })
                            .setStartTime(sehri_hour, sehri_min)
                            .setDoneText("Ok")
                            .setCancelText("Cancel")
                            .setThemeLight();
                    rtpd.show(getActivity().getSupportFragmentManager(), FRAG_TAG_TIME_PICKER);;

                } else {
                    Toast.makeText(getContext(), "রমজান শুরু হয়নি", Toast.LENGTH_SHORT).show();
                }
            }
        });

        iftar_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (match == 1) {
                    RadialTimePickerDialogFragment rtpd = new RadialTimePickerDialogFragment()
                            .setOnTimeSetListener(new RadialTimePickerDialogFragment.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
                                    i.putExtra(AlarmClock.EXTRA_MESSAGE, "ইফতারের সময় হয়েছে");
                                    i.putExtra(AlarmClock.EXTRA_HOUR, hourOfDay);
                                    i.putExtra(AlarmClock.EXTRA_MINUTES, minute);
                                    i.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
                                    startActivity(i);
                                }
                            })
                            .setStartTime(iftar_hour, iftar_min)
                            .setDoneText("Ok")
                            .setCancelText("Cancel")
                            .setThemeLight();
                    rtpd.show(getActivity().getSupportFragmentManager(), FRAG_TAG_TIME_PICKER);;
                } else {
                    Toast.makeText(getContext(), "রমজান শুরু হয়নি", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

}

