package bytelabapps.ramadansalattime.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import bytelabapps.ramadansalattime.R;


public class DeveloperFragment extends Fragment {
    TextView tv, tv2, tv3, tv4, tv5, tv6;

    @Nullable


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.fragment_developer, null);

        String versionName = "";
        try {
            versionName = getActivity().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        TextView versionname = (TextView)v.findViewById(R.id.version_id);
        versionname.setText(versionName);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("back", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("now","dev");
        editor.apply();
        return v;
    }


}