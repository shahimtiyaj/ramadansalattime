package bytelabapps.ramadansalattime.fragment;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import bytelabapps.ramadansalattime.R;


public class FragmentTasbeeh extends Fragment {

    int counter;
    int x;
    MediaPlayer mediaPlayer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.layout_tasbeeh, null);

        CardView tap = (CardView) view.findViewById(R.id.tap);
        final TextView count = (TextView) view.findViewById(R.id.count);
        final TextView total = (TextView) view.findViewById(R.id.total);
        final Vibrator vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        Button reset = (Button) view.findViewById(R.id.reset);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter = 0;
                x = 0;
                count.setText(String.valueOf(0));
                total.setText(String.valueOf("মোট: "+0));
            }
        });
        counter = 0;
        x = 0;

       //mediaPlayer = MediaPlayer.create(getContext(), R.raw.tick);

        count.setText(String.valueOf(counter));
        total.setText(String.valueOf("মোট: " + counter));
        tap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                managerOfSound();
                x = x + 1;
                counter = counter + 1;
                if (counter == 33) {
                    vibrator.vibrate(300);
                    count.setText(String.valueOf(counter));

                } else if (counter > 33) {
                    counter = 1;
                    count.setText(String.valueOf(counter));
                } else {
                    count.setText(String.valueOf(counter));
                }

                total.setText("মোট: " + x);
            }
        });

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    private void managerOfSound() {
        mediaPlayer = MediaPlayer.create(getContext(), R.raw.tick);
        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.start();
        } else {
            mediaPlayer.stop();
        }
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.reset();
                mp.release();
            }
        });
    }

}



