package bytelabapps.ramadansalattime.helper;


import android.content.SharedPreferences;

/**
 * Created by Sajid on 5/19/2016.
 */
public class GetLocation {
    private double latitude;
    private double longitude;
    private SharedPreferences sharedPreferences;

    public GetLocation(SharedPreferences mPrefs) {
        this.sharedPreferences = mPrefs;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        switch (sharedPreferences.getString("district","")) {
            case "ঢাকা":
                editor.putLong("lat", Double.doubleToRawLongBits(23.7104));
                editor.putLong("lon", Double.doubleToRawLongBits(90.40744));
                editor.apply();
                break;
            case "ফরিদপুর":
                editor.putLong("lat", Double.doubleToRawLongBits(23.60612));
                editor.putLong("lon", Double.doubleToRawLongBits(89.84064));
                editor.apply();
                break;
            case "গাজীপুর":
                editor.putLong("lat", Double.doubleToRawLongBits(24.0958));
                editor.putLong("lon", Double.doubleToRawLongBits(90.4125));
                editor.apply();
                break;
            case "গোপালগঞ্জ":
                editor.putLong("lat", Double.doubleToRawLongBits(23.0488));
                editor.putLong("lon", Double.doubleToRawLongBits(89.8879));
                editor.apply();
                break;
            case "কিশোরগঞ্জ":
                editor.putLong("lat", Double.doubleToRawLongBits(24.43944));
                editor.putLong("lon", Double.doubleToRawLongBits(90.78291));
                editor.apply();
                break;
            case "মাদারীপুর":
                editor.putLong("lat", Double.doubleToRawLongBits(23.1709));
                editor.putLong("lon", Double.doubleToRawLongBits(90.20935));
                editor.apply();
                break;
            case "মানিকগঞ্জ":
                editor.putLong("lat", Double.doubleToRawLongBits(23.8585));
                editor.putLong("lon", Double.doubleToRawLongBits(89.9253));
                editor.apply();
                break;
            case "মুন্সীগঞ্জ":
                editor.putLong("lat", Double.doubleToRawLongBits(23.5249));
                editor.putLong("lon", Double.doubleToRawLongBits(90.3372));
                editor.apply();
                break;
            case "নারায়ণগঞ্জ":
                editor.putLong("lat", Double.doubleToRawLongBits(23.61352));
                editor.putLong("lon", Double.doubleToRawLongBits(90.50298));
                editor.apply();
                break;
            case "নরসিংদী":
                editor.putLong("lat", Double.doubleToRawLongBits(23.92298));
                editor.putLong("lon", Double.doubleToRawLongBits(90.71768));
                editor.apply();
                break;
            case "রাজবাড়ী":
                editor.putLong("lat", Double.doubleToRawLongBits(23.7151));
                editor.putLong("lon", Double.doubleToRawLongBits(89.5875));
                editor.apply();
                break;
            case "শরীয়তপুর":
                editor.putLong("lat", Double.doubleToRawLongBits(23.2866));
                editor.putLong("lon", Double.doubleToRawLongBits(90.3748));
                editor.apply();
                break;
            case "টাঙ্গাইল":
                editor.putLong("lat", Double.doubleToRawLongBits(24.24984));
                editor.putLong("lon", Double.doubleToRawLongBits(89.91655));
                editor.apply();
                break;
            case "চট্টগ্রাম":
                editor.putLong("lat", Double.doubleToRawLongBits(22.3384));
                editor.putLong("lon", Double.doubleToRawLongBits(91.83168));
                editor.apply();
                break;
            case "বান্দরবান":
                editor.putLong("lat", Double.doubleToRawLongBits(22.19534));
                editor.putLong("lon", Double.doubleToRawLongBits(92.21946));
                editor.apply();
                break;
            case "ব্রাহ্মণবাড়ীয়া":
                editor.putLong("lat", Double.doubleToRawLongBits(23.9616));
                editor.putLong("lon", Double.doubleToRawLongBits(91.1065));
                editor.apply();
                break;
            case "চাঁদপুর":
                editor.putLong("lat", Double.doubleToRawLongBits(23.2849));
                editor.putLong("lon", Double.doubleToRawLongBits(90.8294));
                editor.apply();
                break;
            case "কুমিল্লা":
                editor.putLong("lat", Double.doubleToRawLongBits(23.46186));
                editor.putLong("lon", Double.doubleToRawLongBits(91.18503));
                editor.apply();
                break;
            case "কক্সবাজার":
                editor.putLong("lat", Double.doubleToRawLongBits(21.45388));
                editor.putLong("lon", Double.doubleToRawLongBits(91.96765));
                editor.apply();
                break;
            case "ফেনী":
                editor.putLong("lat", Double.doubleToRawLongBits(23.0144));
                editor.putLong("lon", Double.doubleToRawLongBits(91.3966));
                editor.apply();
                break;
            case "খাগড়াছড়ি":
                editor.putLong("lat", Double.doubleToRawLongBits(23.10787));
                editor.putLong("lon", Double.doubleToRawLongBits(91.97007));
                editor.apply();
                break;
            case "লক্ষীপুর":
                editor.putLong("lat", Double.doubleToRawLongBits(22.9443));
                editor.putLong("lon", Double.doubleToRawLongBits(90.83005));
                editor.apply();
                break;
            case "নোয়াখালী":
                editor.putLong("lat", Double.doubleToRawLongBits(22.5205));
                editor.putLong("lon", Double.doubleToRawLongBits(91.1353));
                editor.apply();
                break;
            case "রাঙ্গামাটি":
                editor.putLong("lat", Double.doubleToRawLongBits(22.8904));
                editor.putLong("lon", Double.doubleToRawLongBits(92.2237));
                editor.apply();
                break;
            case "রাজশাহী":
                editor.putLong("lat", Double.doubleToRawLongBits(24.374));
                editor.putLong("lon", Double.doubleToRawLongBits(88.60114));
                editor.apply();
                break;
            case "বগুড়া":
                editor.putLong("lat", Double.doubleToRawLongBits(24.85098));
                editor.putLong("lon", Double.doubleToRawLongBits(89.37108));
                editor.apply();
                break;
            case "জয়পুরহাট":
                editor.putLong("lat", Double.doubleToRawLongBits(25.10147));
                editor.putLong("lon", Double.doubleToRawLongBits(89.02734));
                editor.apply();
                break;
            case "নওগাঁ":
                editor.putLong("lat", Double.doubleToRawLongBits(24.7936));
                editor.putLong("lon", Double.doubleToRawLongBits(88.9318));
                editor.apply();
                break;
            case "নাটোর":
                editor.putLong("lat", Double.doubleToRawLongBits(24.4260));
                editor.putLong("lon", Double.doubleToRawLongBits(89.0179));
                editor.apply();
                break;
            case "নওয়াবগঞ্জ":
                editor.putLong("lat", Double.doubleToRawLongBits(24.59025));
                editor.putLong("lon", Double.doubleToRawLongBits(88.27444));
                editor.apply();
                break;
            case "পাবনা":
                editor.putLong("lat", Double.doubleToRawLongBits(24.00644));
                editor.putLong("lon", Double.doubleToRawLongBits(89.2372));
                editor.apply();
                break;
            case "সিরাজগঞ্জ":
                editor.putLong("lat", Double.doubleToRawLongBits(24.45771));
                editor.putLong("lon", Double.doubleToRawLongBits(89.70802));
                editor.apply();
                break;
            case "খুলনা":
                editor.putLong("lat", Double.doubleToRawLongBits(22.80979));
                editor.putLong("lon", Double.doubleToRawLongBits(89.56439));
                editor.apply();
                break;
            case "বাগেরহাট":
                editor.putLong("lat", Double.doubleToRawLongBits(22.3336));
                editor.putLong("lon", Double.doubleToRawLongBits(89.7755));
                editor.apply();
                break;
            case "চুয়াডাঙ্গা":
                editor.putLong("lat", Double.doubleToRawLongBits(23.6161));
                editor.putLong("lon", Double.doubleToRawLongBits(88.8263));
                editor.apply();
                break;
            case "যশোর":
                editor.putLong("lat", Double.doubleToRawLongBits(23.16971));
                editor.putLong("lon", Double.doubleToRawLongBits(89.21371));
                editor.apply();
                break;
            case "ঝিনাইদহ":
                editor.putLong("lat", Double.doubleToRawLongBits(23.4754));
                editor.putLong("lon", Double.doubleToRawLongBits(89.1706));
                editor.apply();
                break;
            case "কুষ্টিয়া":
                editor.putLong("lat", Double.doubleToRawLongBits(23.9028));
                editor.putLong("lon", Double.doubleToRawLongBits(89.11943));
                editor.apply();
                break;
            case "মাগুরা":
                editor.putLong("lat", Double.doubleToRawLongBits(23.4290));
                editor.putLong("lon", Double.doubleToRawLongBits(89.4364));
                editor.apply();
                break;
            case "মেহেরপুর":
                editor.putLong("lat", Double.doubleToRawLongBits(23.8052));
                editor.putLong("lon", Double.doubleToRawLongBits(88.6724));
                editor.apply();
                break;
            case "নড়াইল":
                editor.putLong("lat", Double.doubleToRawLongBits(23.15509));
                editor.putLong("lon", Double.doubleToRawLongBits(89.49515));
                editor.apply();
                break;
            case "সাতক্ষিরা":
                editor.putLong("lat", Double.doubleToRawLongBits(22.70817));
                editor.putLong("lon", Double.doubleToRawLongBits(89.07185));
                editor.apply();
                break;
            case "বরিশাল":
                editor.putLong("lat", Double.doubleToRawLongBits(22.70497));
                editor.putLong("lon", Double.doubleToRawLongBits(90.37013));
                editor.apply();
                break;
            case "বরগুনা":
                editor.putLong("lat", Double.doubleToRawLongBits(22.0953));
                editor.putLong("lon", Double.doubleToRawLongBits(90.1121));
                editor.apply();
                break;
            case "ভোলা":
                editor.putLong("lat", Double.doubleToRawLongBits(22.68759));
                editor.putLong("lon", Double.doubleToRawLongBits(90.64403));
                editor.apply();
                break;
            case "ঝালকাঠি":
                editor.putLong("lat", Double.doubleToRawLongBits(22.5721));
                editor.putLong("lon", Double.doubleToRawLongBits(90.1870));
                editor.apply();
                break;
            case "পটুয়াখালী":
                editor.putLong("lat", Double.doubleToRawLongBits(22.3543));
                editor.putLong("lon", Double.doubleToRawLongBits(90.3348));
                editor.apply();
                break;
            case "পিরোজপুর":
                editor.putLong("lat", Double.doubleToRawLongBits(22.57965));
                editor.putLong("lon", Double.doubleToRawLongBits(89.97521));
                editor.apply();
                break;
            case "সিলেট":
                editor.putLong("lat", Double.doubleToRawLongBits(24.89904));
                editor.putLong("lon", Double.doubleToRawLongBits(91.87198));
                editor.apply();
                break;
            case "হবিগঞ্জ":
                editor.putLong("lat", Double.doubleToRawLongBits(24.38044));
                editor.putLong("lon", Double.doubleToRawLongBits(91.41299));
                editor.apply();
                break;
            case "মৌলভীবাজার":
                editor.putLong("lat", Double.doubleToRawLongBits(24.48888));
                editor.putLong("lon", Double.doubleToRawLongBits(91.77075));
                editor.apply();
                break;
            case "সুনামগঞ্জ":
                editor.putLong("lat", Double.doubleToRawLongBits(25.0667));
                editor.putLong("lon", Double.doubleToRawLongBits(91.4072));
                editor.apply();
                break;
            case "রংপুর":
                editor.putLong("lat", Double.doubleToRawLongBits(25.74664));
                editor.putLong("lon", Double.doubleToRawLongBits(89.25166));
                editor.apply();
                break;
            case "দিনাজপুর":
                editor.putLong("lat", Double.doubleToRawLongBits(25.62745));
                editor.putLong("lon", Double.doubleToRawLongBits(88.63779));
                editor.apply();
                break;
            case "গাইবান্ধা":
                editor.putLong("lat", Double.doubleToRawLongBits(25.1880));
                editor.putLong("lon", Double.doubleToRawLongBits(89.4742));
                editor.apply();
                break;
            case "কুড়িগ্রাম":
                editor.putLong("lat", Double.doubleToRawLongBits(25.7570));
                editor.putLong("lon", Double.doubleToRawLongBits(89.6252));
                editor.apply();
                break;
            case "লালমনিরহাট":
                editor.putLong("lat", Double.doubleToRawLongBits(25.91719));
                editor.putLong("lon", Double.doubleToRawLongBits(89.44595));
                editor.apply();
                break;
            case "নীলফামারী":
                editor.putLong("lat", Double.doubleToRawLongBits(25.9900));
                editor.putLong("lon", Double.doubleToRawLongBits(88.9031));
                editor.apply();
                break;
            case "পঞ্চগড়":
                editor.putLong("lat", Double.doubleToRawLongBits(26.33338));
                editor.putLong("lon", Double.doubleToRawLongBits(88.55777));
                editor.apply();
                break;
            case "ঠাকুরগাঁও":
                editor.putLong("lat", Double.doubleToRawLongBits(26.03097));
                editor.putLong("lon", Double.doubleToRawLongBits(88.46989));
                editor.apply();
                break;
            case "ময়মনসিংহ":
                editor.putLong("lat", Double.doubleToRawLongBits(24.75636));
                editor.putLong("lon", Double.doubleToRawLongBits(90.40646));
                editor.apply();
                break;
            case "জামালপুর":
                editor.putLong("lat", Double.doubleToRawLongBits(24.91965));
                editor.putLong("lon", Double.doubleToRawLongBits(89.94812));
                editor.apply();
                break;
            case "নেত্রকোনা":
                editor.putLong("lat", Double.doubleToRawLongBits(24.88352));
                editor.putLong("lon", Double.doubleToRawLongBits(90.72898));
                editor.apply();
                break;
            case "শেরপুর":
                editor.putLong("lat", Double.doubleToRawLongBits(25.01881));
                editor.putLong("lon", Double.doubleToRawLongBits(90.01751));
                editor.apply();
                break;
        }
    }

    public Object getLatitude() {
        this.latitude = Double.longBitsToDouble(sharedPreferences.getLong("lat", Double.doubleToLongBits(0)));
        return latitude;

    }

    public Object getLongitude() {
        this.longitude = Double.longBitsToDouble(sharedPreferences.getLong("lon", Double.doubleToLongBits(0)));
        return longitude;
    }
}
