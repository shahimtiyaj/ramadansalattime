package bytelabapps.ramadansalattime.helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import bytelabapps.ramadansalattime.R;


/**
 * Created by Shah on 5/17/2016.
 */
public class GetTime {

    private String[] sehri_time;
    private String[] iftar_time;


    public GetTime(Activity activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences("division", Context.MODE_PRIVATE);
        switch (sharedPreferences.getString("district", "")) {
            case "ঢাকা":
                this.sehri_time = activity.getResources().getStringArray(R.array.dhaka_narayangonj_chadpur_noakhali_munshigonj_ponchoghor_nifamari__sahri);
                this.iftar_time = activity.getResources().getStringArray(R.array.dhaka_narayangonj_gazipur_firojpur_madaripur_kishorgonj_iftar);
                break;
            case "ফরিদপুর":
                this.sehri_time = activity.getResources().getStringArray(R.array.madaripur_barishal_faridpur_noaga_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.manikgonj_khulna_moymonsing_faridpur_ifter);
                break;
            case "গাজীপুর":
                this.sehri_time = activity.getResources().getStringArray(R.array.gajipur_norshingdi_jamalpur_chittagong_rongpur_gaibandha_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.dhaka_narayangonj_gazipur_firojpur_madaripur_kishorgonj_iftar);
                break;
            case "গোপালগঞ্জ":
                this.sehri_time = activity.getResources().getStringArray(R.array.gopalgonj_kustia_rajbari_magura_putuakhali_rajshahi_firojpur_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.gopalgonj_netrokona_bagerhat_ifter);
                break;
            case "কিশোরগঞ্জ":
                this.sehri_time = activity.getResources().getStringArray(R.array.mymenshing_rangamati_kishorgonj_bandorbon_bramonbaria_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.dhaka_narayangonj_gazipur_firojpur_madaripur_kishorgonj_iftar);
                break;
            case "মাদারীপুর":
                this.sehri_time = activity.getResources().getStringArray(R.array.madaripur_barishal_faridpur_noaga_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.dhaka_narayangonj_gazipur_firojpur_madaripur_kishorgonj_iftar);
                break;
            case "মানিকগঞ্জ":
                this.sehri_time = activity.getResources().getStringArray(R.array.manikgonj_vola_soriyotpur_dinajpur_thakurgoan_joupurhat_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.manikgonj_khulna_moymonsing_faridpur_ifter);
                break;
            case "মুন্সীগঞ্জ":
                this.sehri_time = activity.getResources().getStringArray(R.array.dhaka_narayangonj_chadpur_noakhali_munshigonj_ponchoghor_nifamari__sahri);
                this.iftar_time = activity.getResources().getStringArray(R.array.soriotpur_munsiganj_norsindi_sunamganj_jalokathi_iftar);
                break;
            case "নারায়ণগঞ্জ":
                this.sehri_time = activity.getResources().getStringArray(R.array.dhaka_narayangonj_chadpur_noakhali_munshigonj_ponchoghor_nifamari__sahri);
                this.iftar_time = activity.getResources().getStringArray(R.array.dhaka_narayangonj_gazipur_firojpur_madaripur_kishorgonj_iftar);
                break;
            case "নরসিংদী":
                this.sehri_time = activity.getResources().getStringArray(R.array.gajipur_norshingdi_jamalpur_chittagong_rongpur_gaibandha_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.soriotpur_munsiganj_norsindi_sunamganj_jalokathi_iftar);
                break;
            case "রাজবাড়ী":
                this.sehri_time = activity.getResources().getStringArray(R.array.gopalgonj_kustia_rajbari_magura_putuakhali_rajshahi_firojpur_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.rajbari_kustia_jinaidha_pabna_iftar);
                break;
            case "শরীয়তপুর":
                this.sehri_time = activity.getResources().getStringArray(R.array.manikgonj_vola_soriyotpur_dinajpur_thakurgoan_joupurhat_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.soriotpur_munsiganj_norsindi_sunamganj_jalokathi_iftar);
                break;
            case "টাঙ্গাইল":
                this.sehri_time = activity.getResources().getStringArray(R.array.tangail_lokhipur_coxbazar_bogura_sirajgonj_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.tangail_norail_iftar);
                break;
            case "চট্টগ্রাম":
                this.sehri_time = activity.getResources().getStringArray(R.array.gajipur_norshingdi_jamalpur_chittagong_rongpur_gaibandha_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.khagrachuri_chittagong_iftar);
                break;
            case "বান্দরবান":
                this.sehri_time = activity.getResources().getStringArray(R.array.mymenshing_rangamati_kishorgonj_bandorbon_bramonbaria_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.bandorban_coxbazar_iftar);
                break;
            case "ব্রাহ্মণবাড়ীয়া":
                this.sehri_time = activity.getResources().getStringArray(R.array.mymenshing_rangamati_kishorgonj_bandorbon_bramonbaria_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.cadpur_borisal_potuakhali_brommonbaria_borguna_ifter);
                break;
            case "চাঁদপুর":
                this.sehri_time = activity.getResources().getStringArray(R.array.dhaka_narayangonj_chadpur_noakhali_munshigonj_ponchoghor_nifamari__sahri);
                this.iftar_time = activity.getResources().getStringArray(R.array.cadpur_borisal_potuakhali_brommonbaria_borguna_ifter);
                break;



            case "কুমিল্লা":
                this.sehri_time = activity.getResources().getStringArray(R.array.sherpur_comilla_feni_kurigram_lalmonirhat_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.shylet_comilla_noakhali_moulovibazar_iftar);
                break;
            case "কক্সবাজার":
                this.sehri_time = activity.getResources().getStringArray(R.array.tangail_lokhipur_coxbazar_bogura_sirajgonj_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.bandorban_coxbazar_iftar);
                break;
            case "ফেনী":
                this.sehri_time = activity.getResources().getStringArray(R.array.sherpur_comilla_feni_kurigram_lalmonirhat_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.feni_ifter);
                break;
            case "খাগড়াছড়ি":
                this.sehri_time = activity.getResources().getStringArray(R.array.hobiganj_khagrachori_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.khagrachuri_chittagong_iftar);
                break;
            case "লক্ষীপুর":
                this.sehri_time = activity.getResources().getStringArray(R.array.tangail_lokhipur_coxbazar_bogura_sirajgonj_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.hobigoj_lokhipur_vola_ifter);
                break;
            case "নোয়াখালী":
                this.sehri_time = activity.getResources().getStringArray(R.array.dhaka_narayangonj_chadpur_noakhali_munshigonj_ponchoghor_nifamari__sahri);
                this.iftar_time = activity.getResources().getStringArray(R.array.shylet_comilla_noakhali_moulovibazar_iftar);
                break;
            case "রাঙ্গামাটি":
                this.sehri_time = activity.getResources().getStringArray(R.array.mymenshing_rangamati_kishorgonj_bandorbon_bramonbaria_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.rangamati_ifter);
                break;
            case "রাজশাহী":
                this.sehri_time = activity.getResources().getStringArray(R.array.gopalgonj_kustia_rajbari_magura_putuakhali_rajshahi_firojpur_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.rajbari_kustia_jinaidha_pabna_iftar);
                break;
            case "বগুড়া":
                this.sehri_time = activity.getResources().getStringArray(R.array.tangail_lokhipur_coxbazar_bogura_sirajgonj_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.gaibandha_bogura_chuadhanga_ifter);
                break;
            case "জয়পুরহাট":
                this.sehri_time = activity.getResources().getStringArray(R.array.manikgonj_vola_soriyotpur_dinajpur_thakurgoan_joupurhat_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.rajsahi_rongpur_nouga_joypurhat_lalmonirhat_ifter);
                break;
            case "নওগাঁ":
                this.sehri_time = activity.getResources().getStringArray(R.array.madaripur_barishal_faridpur_noaga_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.rajsahi_rongpur_nouga_joypurhat_lalmonirhat_ifter);
                break;
            case "নাটোর":
                this.sehri_time = activity.getResources().getStringArray(R.array.jalkhati_nator_pabana_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.meherpur_nator_kurigram_iftar);
                break;


            case "নওয়াবগঞ্জ":
                this.sehri_time = activity.getResources().getStringArray(R.array.chapaibabgonz_bagerhat_jinaidha_borguna_norail_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.nilpamari_dinajpur_capainobabgpnj_iftar);
                break;
            case "পাবনা":
                this.sehri_time = activity.getResources().getStringArray(R.array.jalkhati_nator_pabana_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.rajbari_kustia_jinaidha_pabna_iftar);
                break;
            case "সিরাজগঞ্জ":
                this.sehri_time = activity.getResources().getStringArray(R.array.tangail_lokhipur_coxbazar_bogura_sirajgonj_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.serpur_jamalpur_jossor_magura_satkhira_sirajganj_ifter);
                break;
            case "খুলনা":
                this.sehri_time = activity.getResources().getStringArray(R.array.khulna_jossor_meherpur_chuadanga_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.manikgonj_khulna_moymonsing_faridpur_ifter);
                break;
            case "বাগেরহাট":
                this.sehri_time = activity.getResources().getStringArray(R.array.chapaibabgonz_bagerhat_jinaidha_borguna_norail_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.gopalgonj_netrokona_bagerhat_ifter);
                break;
            case "চুয়াডাঙ্গা":
                this.sehri_time = activity.getResources().getStringArray(R.array.khulna_jossor_meherpur_chuadanga_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.gaibandha_bogura_chuadhanga_ifter);
                break;
            case "যশোর":
                this.sehri_time = activity.getResources().getStringArray(R.array.khulna_jossor_meherpur_chuadanga_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.serpur_jamalpur_jossor_magura_satkhira_sirajganj_ifter);
                break;
            case "ঝিনাইদহ":
                this.sehri_time = activity.getResources().getStringArray(R.array.chapaibabgonz_bagerhat_jinaidha_borguna_norail_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.rajbari_kustia_jinaidha_pabna_iftar);
                break;
            case "কুষ্টিয়া":
                this.sehri_time = activity.getResources().getStringArray(R.array.gopalgonj_kustia_rajbari_magura_putuakhali_rajshahi_firojpur_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.rajbari_kustia_jinaidha_pabna_iftar);
                break;
            case "মাগুরা":
                this.sehri_time = activity.getResources().getStringArray(R.array.gopalgonj_kustia_rajbari_magura_putuakhali_rajshahi_firojpur_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.serpur_jamalpur_jossor_magura_satkhira_sirajganj_ifter);
                break;
            case "মেহেরপুর":
                this.sehri_time = activity.getResources().getStringArray(R.array.khulna_jossor_meherpur_chuadanga_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.meherpur_nator_kurigram_iftar);
                break;
            case "নড়াইল":
                this.sehri_time = activity.getResources().getStringArray(R.array.chapaibabgonz_bagerhat_jinaidha_borguna_norail_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.tangail_norail_iftar);
                break;
            case "সাতক্ষিরা":
                this.sehri_time = activity.getResources().getStringArray(R.array.satkhira_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.serpur_jamalpur_jossor_magura_satkhira_sirajganj_ifter);
                break;
            case "বরিশাল":
                this.sehri_time = activity.getResources().getStringArray(R.array.madaripur_barishal_faridpur_noaga_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.cadpur_borisal_potuakhali_brommonbaria_borguna_ifter);
                break;
            case "বরগুনা":
                this.sehri_time = activity.getResources().getStringArray(R.array.chapaibabgonz_bagerhat_jinaidha_borguna_norail_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.cadpur_borisal_potuakhali_brommonbaria_borguna_ifter);
                break;
            case "ভোলা":
                this.sehri_time = activity.getResources().getStringArray(R.array.manikgonj_vola_soriyotpur_dinajpur_thakurgoan_joupurhat_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.hobigoj_lokhipur_vola_ifter);
                break;
            case "ঝালকাঠি":
                this.sehri_time = activity.getResources().getStringArray(R.array.jalkhati_nator_pabana_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.soriotpur_munsiganj_norsindi_sunamganj_jalokathi_iftar);
                break;
            case "পটুয়াখালী":
                this.sehri_time = activity.getResources().getStringArray(R.array.gopalgonj_kustia_rajbari_magura_putuakhali_rajshahi_firojpur_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.cadpur_borisal_potuakhali_brommonbaria_borguna_ifter);
                break;
            case "পিরোজপুর":
                this.sehri_time = activity.getResources().getStringArray(R.array.gopalgonj_kustia_rajbari_magura_putuakhali_rajshahi_firojpur_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.dhaka_narayangonj_gazipur_firojpur_madaripur_kishorgonj_iftar);
                break;
            case "সিলেট":
                this.sehri_time = activity.getResources().getStringArray(R.array.sylet_sahri);
                this.iftar_time = activity.getResources().getStringArray(R.array.shylet_comilla_noakhali_moulovibazar_iftar);
                break;
            case "হবিগঞ্জ":
                this.sehri_time = activity.getResources().getStringArray(R.array.hobiganj_khagrachori_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.hobigoj_lokhipur_vola_ifter);
                break;
            case "মৌলভীবাজার":
                this.sehri_time = activity.getResources().getStringArray(R.array.moulobibazzar_sunamganj_sahri);
                this.iftar_time = activity.getResources().getStringArray(R.array.shylet_comilla_noakhali_moulovibazar_iftar);
                break;
            case "সুনামগঞ্জ":
                this.sehri_time = activity.getResources().getStringArray(R.array.moulobibazzar_sunamganj_sahri);
                this.iftar_time = activity.getResources().getStringArray(R.array.soriotpur_munsiganj_norsindi_sunamganj_jalokathi_iftar);
                break;
            case "রংপুর":
                this.sehri_time = activity.getResources().getStringArray(R.array.gajipur_norshingdi_jamalpur_chittagong_rongpur_gaibandha_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.rajsahi_rongpur_nouga_joypurhat_lalmonirhat_ifter);
                break;
            case "দিনাজপুর":
                this.sehri_time = activity.getResources().getStringArray(R.array.manikgonj_vola_soriyotpur_dinajpur_thakurgoan_joupurhat_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.nilpamari_dinajpur_capainobabgpnj_iftar);
                break;
            case "গাইবান্ধা":
                this.sehri_time = activity.getResources().getStringArray(R.array.gajipur_norshingdi_jamalpur_chittagong_rongpur_gaibandha_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.gaibandha_bogura_chuadhanga_ifter);
                break;
            case "কুড়িগ্রাম":
                this.sehri_time = activity.getResources().getStringArray(R.array.sherpur_comilla_feni_kurigram_lalmonirhat_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.meherpur_nator_kurigram_iftar);
                break;
            case "লালমনিরহাট":
                this.sehri_time = activity.getResources().getStringArray(R.array.sherpur_comilla_feni_kurigram_lalmonirhat_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.rajsahi_rongpur_nouga_joypurhat_lalmonirhat_ifter);
                break;
            case "নীলফামারী":
                this.sehri_time = activity.getResources().getStringArray(R.array.dhaka_narayangonj_chadpur_noakhali_munshigonj_ponchoghor_nifamari__sahri);
                this.iftar_time = activity.getResources().getStringArray(R.array.nilpamari_dinajpur_capainobabgpnj_iftar);
                break;
            case "পঞ্চগড়":
                this.sehri_time = activity.getResources().getStringArray(R.array.dhaka_narayangonj_chadpur_noakhali_munshigonj_ponchoghor_nifamari__sahri);
                this.iftar_time = activity.getResources().getStringArray(R.array.poncogor_thakurgaon_ifter);
                break;
            case "ঠাকুরগাঁও":
                this.sehri_time = activity.getResources().getStringArray(R.array.manikgonj_vola_soriyotpur_dinajpur_thakurgoan_joupurhat_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.poncogor_thakurgaon_ifter);
                break;
            case "ময়মনসিংহ":
                this.sehri_time = activity.getResources().getStringArray(R.array.mymenshing_rangamati_kishorgonj_bandorbon_bramonbaria_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.manikgonj_khulna_moymonsing_faridpur_ifter);
                break;
            case "জামালপুর":
                this.sehri_time = activity.getResources().getStringArray(R.array.gajipur_norshingdi_jamalpur_chittagong_rongpur_gaibandha_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.serpur_jamalpur_jossor_magura_satkhira_sirajganj_ifter);
                break;
            case "নেত্রকোনা":
                this.sehri_time = activity.getResources().getStringArray(R.array.netrokona_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.gopalgonj_netrokona_bagerhat_ifter);
                break;
            case "শেরপুর":
                this.sehri_time = activity.getResources().getStringArray(R.array.sherpur_comilla_feni_kurigram_lalmonirhat_sahre);
                this.iftar_time = activity.getResources().getStringArray(R.array.serpur_jamalpur_jossor_magura_satkhira_sirajganj_ifter);
                break;
            default:
                break;

        }

    }

    public Object getSehri() {
        return sehri_time;
    }

    public Object getIftar() {
        return iftar_time;
    }
}
