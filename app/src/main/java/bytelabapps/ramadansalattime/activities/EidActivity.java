package bytelabapps.ramadansalattime.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import bytelabapps.ramadansalattime.R;

import static bytelabapps.ramadansalattime.activities.MainActivity.AD_UNIT_ID;


/**
 * Created by shahimtiyaj on 4/30/2016.
 */
public class EidActivity extends AppCompatActivity{




    TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12,
            tv13, tv14, tv15, tv16, tv17, tv18, tv19, tv20, tv21, tv22, tv23,
            tv24, tv25, tv26, tv27, tv28, tv29, tv30, tv31, tv32, tv33, tv34,
            tv35,tv36, tv37, tv38, tv39, tv40, tv41, tv42, tv43, tv44, tv45,
            tv46, tv47, tv48, tv49, tv50, tv51, tv52, tv53, tv54;
    ImageView im;

    int pos = -1;

    //ADMOB Initilization

   // static final String AD_UNIT_ID = "ca-app-pub-8444650851472007/5732111373";

    AdView adView;
    LinearLayout linearLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_eid);
        // Show the Actionbar in the activity
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.app_name));

        SharedPreferences sharedPreferences = getSharedPreferences("back",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("now","kej_details");
        editor.apply();

//        //Circular ImageView
//        Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.kodor);
//        Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 100);
//
//        ImageView circularImageView = (ImageView) findViewById(R.id.imageView);
//        circularImageView.setImageBitmap(circularBitmap);

        tv1 = (TextView) findViewById(R.id.textss1);
        tv2 = (TextView) findViewById(R.id.textss2);
        tv3 = (TextView) findViewById(R.id.textss3);
        tv4 = (TextView) findViewById(R.id.textss4);
        tv5 = (TextView) findViewById(R.id.textss5);

        tv6 = (TextView) findViewById(R.id.textss6);
        tv7 = (TextView) findViewById(R.id.textss7);
        tv8 = (TextView) findViewById(R.id.textss8);
        tv9 = (TextView) findViewById(R.id.textss9);
        tv10 = (TextView) findViewById(R.id.textss10);

        tv11 = (TextView) findViewById(R.id.textss11);
        tv12 = (TextView) findViewById(R.id.textss12);
        tv13 = (TextView) findViewById(R.id.textss13);
        tv14 = (TextView) findViewById(R.id.textss14);
        tv15 = (TextView) findViewById(R.id.textss15);

        tv16 = (TextView) findViewById(R.id.textss16);
        tv17 = (TextView) findViewById(R.id.textss17);
        tv18 = (TextView) findViewById(R.id.textss18);
        tv19 = (TextView) findViewById(R.id.textss19);
        tv20 = (TextView) findViewById(R.id.textss20);

        tv21 = (TextView) findViewById(R.id.textss21);
        tv22 = (TextView) findViewById(R.id.textss22);
        tv23 = (TextView) findViewById(R.id.textss23);
        tv24 = (TextView) findViewById(R.id.textss24);
        tv25 = (TextView) findViewById(R.id.textss25);

        tv26 = (TextView) findViewById(R.id.textss26);
        tv27 = (TextView) findViewById(R.id.textss27);
        tv28 = (TextView) findViewById(R.id.textss28);
        tv29 = (TextView) findViewById(R.id.textss29);
        tv30 = (TextView) findViewById(R.id.textss30);
        tv31 = (TextView) findViewById(R.id.textss31);

        tv32 = (TextView) findViewById(R.id.textss32);
        tv33 = (TextView) findViewById(R.id.textss33);
        tv34 = (TextView) findViewById(R.id.textss34);
        tv35 = (TextView) findViewById(R.id.textss35);



        tv36 = (TextView) findViewById(R.id.textss36);
        tv37 = (TextView) findViewById(R.id.textss37);
        tv38 = (TextView) findViewById(R.id.textss38);

        // ------------------------------------------

        tv39 = (TextView) findViewById(R.id.textss39);
        tv40 = (TextView) findViewById(R.id.textss40);
        tv41 = (TextView) findViewById(R.id.textss41);
        tv42 = (TextView) findViewById(R.id.textss42);
        tv43 = (TextView) findViewById(R.id.textss43);
        tv44 = (TextView) findViewById(R.id.textss44);
        tv45 = (TextView) findViewById(R.id.textss45);
        tv46 = (TextView) findViewById(R.id.textss46);
        tv47 = (TextView) findViewById(R.id.textss47);
        tv48 = (TextView) findViewById(R.id.textss48);
        tv49 = (TextView) findViewById(R.id.textss49);
        tv50 = (TextView) findViewById(R.id.textss50);
        tv51 = (TextView) findViewById(R.id.textss51);
        tv52 = (TextView) findViewById(R.id.textss52);

        tv53 = (TextView) findViewById(R.id.textss53);
        tv54 = (TextView) findViewById(R.id.textss54);

        // ---------------------------------------------------------------

        getIntent().getExtras().getString("head");
        tv1.setText("" + getIntent().getStringExtra("hd"));

        getIntent().getExtras().getString("str1");
        tv2.setText("" + getIntent().getStringExtra("st1"));

        getIntent().getExtras().getString("str2");
        tv3.setText("" + getIntent().getStringExtra("st2"));

        getIntent().getExtras().getString("str3");
        tv4.setText("" + getIntent().getStringExtra("st3"));

        getIntent().getExtras().getString("str4");
        tv5.setText("" + getIntent().getStringExtra("st4"));

        getIntent().getExtras().getString("str5");
        tv6.setText("" + getIntent().getStringExtra("st5"));

        getIntent().getExtras().getString("str6");
        tv7.setText("" + getIntent().getStringExtra("st6"));

        getIntent().getExtras().getString("str7");
        tv8.setText("" + getIntent().getStringExtra("st7"));

        getIntent().getExtras().getString("str8");
        tv9.setText("" + getIntent().getStringExtra("st8"));

        getIntent().getExtras().getString("str9");
        tv10.setText("" + getIntent().getStringExtra("st9"));

        getIntent().getExtras().getString("str10");
        tv11.setText("" + getIntent().getStringExtra("st10"));

        getIntent().getExtras().getString("str11");
        tv12.setText("" + getIntent().getStringExtra("st11"));

        getIntent().getExtras().getString("str12");
        tv13.setText("" + getIntent().getStringExtra("st12"));

        getIntent().getExtras().getString("str13");
        tv14.setText("" + getIntent().getStringExtra("st13"));

        getIntent().getExtras().getString("str14");
        tv15.setText("" + getIntent().getStringExtra("st14"));

        getIntent().getExtras().getString("str15");
        tv16.setText("" + getIntent().getStringExtra("st15"));

        getIntent().getExtras().getString("str16");
        tv17.setText("" + getIntent().getStringExtra("st16"));

        getIntent().getExtras().getString("str17");
        tv18.setText("" + getIntent().getStringExtra("st17"));

        getIntent().getExtras().getString("str18");
        tv19.setText("" + getIntent().getStringExtra("st18"));

        getIntent().getExtras().getString("str19");
        tv20.setText("" + getIntent().getStringExtra("st19"));

        getIntent().getExtras().getString("str20");
        tv21.setText("" + getIntent().getStringExtra("st20"));

        getIntent().getExtras().getString("str21");
        tv22.setText("" + getIntent().getStringExtra("st21"));

        getIntent().getExtras().getString("str22");
        tv23.setText("" + getIntent().getStringExtra("st22"));

        getIntent().getExtras().getString("str23");
        tv24.setText("" + getIntent().getStringExtra("st23"));

        getIntent().getExtras().getString("str24");
        tv25.setText("" + getIntent().getStringExtra("st24"));

        getIntent().getExtras().getString("str25");
        tv26.setText("" + getIntent().getStringExtra("st25"));

        getIntent().getExtras().getString("str26");
        tv27.setText("" + getIntent().getStringExtra("st26"));

        getIntent().getExtras().getString("str27");
        tv28.setText("" + getIntent().getStringExtra("st27"));

        getIntent().getExtras().getString("str28");
        tv29.setText("" + getIntent().getStringExtra("st28"));

        getIntent().getExtras().getString("str29");
        tv30.setText("" + getIntent().getStringExtra("st29"));

        getIntent().getExtras().getString("str30");
        tv31.setText("" + getIntent().getStringExtra("st30"));

        getIntent().getExtras().getString("str31");
        tv32.setText("" + getIntent().getStringExtra("st31"));

        getIntent().getExtras().getString("str32");
        tv33.setText("" + getIntent().getStringExtra("st32"));

        getIntent().getExtras().getString("str33");
        tv34.setText("" + getIntent().getStringExtra("st33"));

        getIntent().getExtras().getString("str34");
        tv35.setText("" + getIntent().getStringExtra("st34"));


        //------------------------------


        getIntent().getExtras().getString("str35");
        tv36.setText("" + getIntent().getStringExtra("st35"));

        getIntent().getExtras().getString("str36");
        tv37.setText("" + getIntent().getStringExtra("st36"));

        getIntent().getExtras().getString("str37");
        tv38.setText("" + getIntent().getStringExtra("st37"));

        getIntent().getExtras().getString("str38");
        tv39.setText("" + getIntent().getStringExtra("st38"));

        getIntent().getExtras().getString("str39");
        tv40.setText("" + getIntent().getStringExtra("st39"));

        getIntent().getExtras().getString("str40");
        tv41.setText("" + getIntent().getStringExtra("st40"));

        getIntent().getExtras().getString("str41");
        tv42.setText("" + getIntent().getStringExtra("st41"));

        getIntent().getExtras().getString("str42");
        tv43.setText("" + getIntent().getStringExtra("st42"));

        getIntent().getExtras().getString("str43");
        tv44.setText("" + getIntent().getStringExtra("st43"));

        getIntent().getExtras().getString("str44");
        tv45.setText("" + getIntent().getStringExtra("st44"));

        getIntent().getExtras().getString("str45");
        tv46.setText("" + getIntent().getStringExtra("st45"));

        getIntent().getExtras().getString("str46");
        tv47.setText("" + getIntent().getStringExtra("st46"));

        getIntent().getExtras().getString("str47");
        tv48.setText("" + getIntent().getStringExtra("st47"));

        getIntent().getExtras().getString("str48");
        tv49.setText("" + getIntent().getStringExtra("st48"));

        getIntent().getExtras().getString("str49");
        tv50.setText("" + getIntent().getStringExtra("st49"));

        getIntent().getExtras().getString("str50");
        tv51.setText("" + getIntent().getStringExtra("st50"));

        getIntent().getExtras().getString("str51");
        tv52.setText("" + getIntent().getStringExtra("st51"));

        getIntent().getExtras().getString("str52");
        tv53.setText("" + getIntent().getStringExtra("st52"));

        getIntent().getExtras().getString("str53");
        tv54.setText("" + getIntent().getStringExtra("st53"));




        pos = getIntent().getExtras().getInt("pos");

        // Admob code start from here
        adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(AD_UNIT_ID);
        linearLayout = (LinearLayout) findViewById(R.id.add_linera);

        if (isNetworkAvailable()) {
            linearLayout.addView(adView);
            AdRequest adRequest = new AdRequest.Builder().addTestDevice(AD_UNIT_ID)
                    .build();
            adView.loadAd(adRequest);
        } else {
            ScrollView sv = (ScrollView)findViewById(R.id.LinearLayout1);
            RelativeLayout.LayoutParams layoutParams =  new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, 4);
            sv.setLayoutParams(layoutParams);

        }
    }




    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(EidActivity.this,MainActivity.class);
        intent.putExtra("now","kej_details");
        startActivity(intent);
        finish();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



}