package bytelabapps.ramadansalattime.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import bytelabapps.ramadansalattime.R;

public class DistrictActivity extends AppCompatActivity {

    ListView lv;
    Context context;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    String[] district_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_district);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        context = this;
        sharedPreferences = getSharedPreferences("division", MODE_PRIVATE);
        editor = sharedPreferences.edit();

        switch (sharedPreferences.getString("division", "")) {
            case "ঢাকা":
                district_list = getResources().getStringArray(R.array.dhaka_districts);
                break;
            case "চট্টগ্রাম":
                district_list = getResources().getStringArray(R.array.chittagong_districts);
                break;
            case "রাজশাহী":
                district_list = getResources().getStringArray(R.array.rajshahi_districts);
                break;
            case "খুলনা":
                district_list = getResources().getStringArray(R.array.khulna_districts);
                break;
            case "বরিশাল":
                district_list = getResources().getStringArray(R.array.barisal_districts);
                break;
            case "সিলেট":
                district_list = getResources().getStringArray(R.array.sylhet_districts);
                break;
            case "রংপুর":
                district_list = getResources().getStringArray(R.array.rangpur_districts);
                break;
            case "ময়মনসিংহ":
                district_list = getResources().getStringArray(R.array.mymensing_districts);
                break;
            default:
                district_list = new String[]{"", ""};
                break;
        }


        lv = (ListView) findViewById(R.id.district_listview);
        lv.setAdapter(new CustomAdapter(this, district_list,editor));
        lv.setDivider(null);


    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(DistrictActivity.this, DivisionActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

class CustomAdapter extends BaseAdapter {
    String[] result;
    Context context;
    SharedPreferences.Editor editor;
    private static LayoutInflater inflater = null;

    public CustomAdapter(DistrictActivity districtActivity, String[] distNameList,SharedPreferences.Editor edit) {
        // TODO Auto-generated constructor stub
        result = distNameList;
        context = districtActivity;
        editor = edit;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView tv;
        CheckBox checkBox;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.district_listview_layout, null);
        holder.tv = (TextView) rowView.findViewById(R.id.district);
        holder.checkBox = (CheckBox) rowView.findViewById(R.id.checkbox);
        holder.tv.setText(result[position]);

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("district", result[position]);
                editor.putBoolean("location_saved", true);
                editor.apply();
                Intent intent = new Intent(context,MainActivity.class);
                context.startActivity(intent);
                ((Activity)context).overridePendingTransition(R.anim.left_in, R.anim.left_out);
                ((Activity)context).finish();
            }
        });
        return rowView;
    }

}


