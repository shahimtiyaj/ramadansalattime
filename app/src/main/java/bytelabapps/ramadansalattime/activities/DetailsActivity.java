package bytelabapps.ramadansalattime.activities;


/**
 * Created by shahimtiyaj on 4/19/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import bytelabapps.ramadansalattime.R;
import bytelabapps.ramadansalattime.fragment.TabFragment;

import static bytelabapps.ramadansalattime.activities.MainActivity.AD_UNIT_ID;

public class DetailsActivity extends AppCompatActivity {
    TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12,
            tv13;

    int pos = -1;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    //ADMOB Initilization

    //static final String AD_UNIT_ID = "ca-app-pub-8444650851472007/5732111373";

    AdView adView;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_details);

        // Show the Actionbar in the activity
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.app_name));

        tv1 = (TextView) findViewById(R.id.textss1);
        tv2 = (TextView) findViewById(R.id.textss2);
        tv3 = (TextView) findViewById(R.id.textss3);
        tv4 = (TextView) findViewById(R.id.textss4);
        tv5 = (TextView) findViewById(R.id.textss5);
        tv6 = (TextView) findViewById(R.id.textss6);
        tv7 = (TextView) findViewById(R.id.textss7);
        tv8 = (TextView) findViewById(R.id.textss8);
        tv9 = (TextView) findViewById(R.id.textss9);
        tv10 = (TextView) findViewById(R.id.textss10);
        tv11 = (TextView) findViewById(R.id.textss11);
        tv12 = (TextView) findViewById(R.id.textss12);
        tv13 = (TextView) findViewById(R.id.textss13);

        getIntent().getExtras().getString("head");
        tv1.setText("" + getIntent().getStringExtra("hd"));

        getIntent().getExtras().getString("str1");
        tv2.setText("" + getIntent().getStringExtra("st1"));

        getIntent().getExtras().getString("str2");
        tv3.setText("" + getIntent().getStringExtra("st2"));

        getIntent().getExtras().getString("str3");
        tv4.setText("" + getIntent().getStringExtra("st3"));

        getIntent().getExtras().getString("str4");
        tv5.setText("" + getIntent().getStringExtra("st4"));

        getIntent().getExtras().getString("str5");
        tv6.setText("" + getIntent().getStringExtra("st5"));

        getIntent().getExtras().getString("str6");
        tv7.setText("" + getIntent().getStringExtra("st6"));

        getIntent().getExtras().getString("str7");
        tv8.setText("" + getIntent().getStringExtra("st7"));

        getIntent().getExtras().getString("str8");
        tv9.setText("" + getIntent().getStringExtra("st8"));

        getIntent().getExtras().getString("str9");
        tv10.setText("" + getIntent().getStringExtra("st9"));

        getIntent().getExtras().getString("str10");
        tv11.setText("" + getIntent().getStringExtra("st10"));

        getIntent().getExtras().getString("str11");
        tv12.setText("" + getIntent().getStringExtra("st11"));

        getIntent().getExtras().getString("str12");
        tv13.setText("" + getIntent().getStringExtra("st12"));


        pos = ((Intent) getIntent()).getExtras().getInt("pos");

        // Admob code start from here
        adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(AD_UNIT_ID);
        linearLayout = (LinearLayout) findViewById(R.id.add_linera);

        if (isNetworkAvailable()) {
            linearLayout.addView(adView);
            AdRequest adRequest = new AdRequest.Builder().addTestDevice(AD_UNIT_ID)
                    .build();
            adView.loadAd(adRequest);
        } else {
            ScrollView sv = (ScrollView)findViewById(R.id.LinearLayout1);
            RelativeLayout.LayoutParams layoutParams =  new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, 4);
            sv.setLayoutParams(layoutParams);

        }

        sharedPreferences = getSharedPreferences("back",Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString("now","details");
        editor.apply();


    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(DetailsActivity.this, MainActivity.class);
        intent.putExtra("now","details");
        startActivity(intent);
        editor.putBoolean("forced",false);
        finish();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}