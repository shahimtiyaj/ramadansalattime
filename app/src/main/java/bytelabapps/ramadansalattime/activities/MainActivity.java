package bytelabapps.ramadansalattime.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import bytelabapps.ramadansalattime.fragment.CalenderFragment;
import bytelabapps.ramadansalattime.fragment.DeveloperFragment;
import bytelabapps.ramadansalattime.fragment.FojilotFragment;
import bytelabapps.ramadansalattime.fragment.Fragment_IfterShereDua;
import bytelabapps.ramadansalattime.fragment.KodorEidZakatFragment;
import bytelabapps.ramadansalattime.fragment.KursiFragment;
import bytelabapps.ramadansalattime.fragment.NameFragment;
import bytelabapps.ramadansalattime.fragment.OthersFragment;
import bytelabapps.ramadansalattime.fragment.RomjanAmolFragment;
import bytelabapps.ramadansalattime.fragment.RomjanFragment;
import bytelabapps.ramadansalattime.fragment.RomjanKoroniyoFragment;
import bytelabapps.ramadansalattime.fragment.RomjanVulFragment;
import bytelabapps.ramadansalattime.fragment.SuraFragment;
import bytelabapps.ramadansalattime.fragment.TabFragment;
import bytelabapps.ramadansalattime.R;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    AppBarLayout mAppBarLayout;
    public static final String AD_UNIT_ID = "ca-app-pub-8444650851472007/2465653181";

    AdView adView;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAppBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAppBarLayout.setElevation(0);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Ramadan & Prayer time");

        SharedPreferences mPref = getSharedPreferences("division", MODE_PRIVATE);
        if (mPref.getBoolean("location_saved", false) != true) {
            Intent intent = new Intent(MainActivity.this, DivisionActivity.class);
            startActivity(intent);
            finish();

        }

        adView = new AdView(MainActivity.this);
        adView.setAdUnitId(AD_UNIT_ID);
        adView.setAdSize(AdSize.BANNER);
        linearLayout = (LinearLayout) findViewById(R.id.add_linera);


        TabFragment tabFragment = new TabFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, tabFragment);
        fragmentTransaction.commit();

        if (isNetworkAvailable()) {
            linearLayout.addView(adView);
            AdRequest adRequest = new AdRequest.Builder().addTestDevice(AD_UNIT_ID)
                    .build();
            adView.loadAd(adRequest);
        } else {
            FrameLayout sv = (FrameLayout) findViewById(R.id.fragment_container);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            layoutParams.setMargins(0, 0, 0, 0);
            sv.setLayoutParams(layoutParams);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            TabFragment tabFragment = new TabFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, tabFragment);
            fragmentTransaction.commit();

        } else if (id == R.id.romjar_niyot) {
            Fragment_IfterShereDua ifterFragment = new Fragment_IfterShereDua();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, ifterFragment).addToBackStack("calfrag");
            fragmentTransaction.commit();

        } else if (id == R.id.romjane_somporkito) {

            RomjanFragment romjanFragment = new RomjanFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, romjanFragment).addToBackStack("calfrag");
            fragmentTransaction.commit();
        } else if (id == R.id.fullcal) {

            CalenderFragment calenderFragment = new CalenderFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, calenderFragment).addToBackStack("calfrag");
            fragmentTransaction.commit();
        } else if (id == R.id.change_division) {
            Intent intent = new Intent(MainActivity.this, DivisionActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.others) {
            OthersFragment othersFragment = new OthersFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, othersFragment).addToBackStack("calfrag");
            fragmentTransaction.commit();
        } else if (id == R.id.developer) {

            DeveloperFragment devFragment = new DeveloperFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, devFragment).addToBackStack("calfrag");
            fragmentTransaction.commit();
        } else if (id == R.id.nav_share) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Ramadan and Prayer times App");
            String sAux = "\nRamadan and Prayer times App\n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id=bytelabapps.ramadansalattime \n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "Select One"));
        } else if (id == R.id.rate) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://details?id=bytelabapps.ramadansalattime"));
            startActivity(intent);

        } else if (id == R.id.nav_feedback) {
            Intent email = new Intent(Intent.ACTION_SEND);
            email.setType("text/email");
            email.putExtra(Intent.EXTRA_EMAIL, new String[]{"shahimtiyaj94@gmail.com"});
            email.putExtra(Intent.EXTRA_SUBJECT, "Feedback about Ramadan Calendar");
            email.putExtra(Intent.EXTRA_TEXT, "Hello, " + "\n");
            startActivity(Intent.createChooser(email, "Send Feedback:"));

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setAlarm(View view) {
        Intent i = new Intent(AlarmClock.ACTION_SET_ALARM);
        i.putExtra(AlarmClock.EXTRA_MESSAGE, "নামাজের সময় হয়েছে");
        startActivity(i);
    }

    public void asmaUlHusna(View view) {
        NameFragment nameFragment = new NameFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, nameFragment).addToBackStack("calfrag");
        fragmentTransaction.commit();
    }

    public void ayatulKursi(View view) {
        KursiFragment kursiFragment = new KursiFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, kursiFragment).addToBackStack("calfrag");
        fragmentTransaction.commit();
    }

    public void namazerSura(View view) {


        SuraFragment suraFragment = new SuraFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, suraFragment);
        fragmentTransaction.commit();

    }

    public void lailatulKodor(View view) {
        KodorEidZakatFragment kodorEidZakatFragment = new KodorEidZakatFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, kodorEidZakatFragment);
        fragmentTransaction.commit();
    }

    public void vulTruti(View view) {
        RomjanVulFragment vulFragment = new RomjanVulFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, vulFragment).addToBackStack("calfrag");
        fragmentTransaction.commit();
    }

    public void tirishAmol(View view) {
        RomjanAmolFragment amolFragment = new RomjanAmolFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, amolFragment).addToBackStack("calfrag");
        fragmentTransaction.commit();
    }

    public void romjanFojilat(View view) {
        FojilotFragment fojilotFragment = new FojilotFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fojilotFragment).addToBackStack("calfrag");
        fragmentTransaction.commit();
    }

    public void koronio(View view) {
        RomjanKoroniyoFragment koroniyoFragment = new RomjanKoroniyoFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, koroniyoFragment).addToBackStack("calfrag");
        fragmentTransaction.commit();
    }

    public void showRate() {
        SharedPreferences sharedPreferences = getSharedPreferences("back", MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();
        final AlertDialog.Builder rateappBuilder = new AlertDialog.Builder(MainActivity.this, R.style.DialogTheme);
        rateappBuilder.setMessage("Please rate Ramadan Calendar and Prayer times App on Google Play Store to give us your valuable feedback!")
                .setTitle("Rate Ramadan and Prayer times App")
                .setPositiveButton("REVIEW", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=bytelabapps.ramadansalattime"));
                        startActivity(intent);
                        editor.putInt("rate", 1);
                        editor.apply();
                    }
                })
                .setNegativeButton("NEVER", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editor.putInt("rate", 1);
                        editor.apply();
                        finish();
                    }
                })
                .setNeutralButton("LATER", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

        rateappBuilder.show();
    }

    public void showExit() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.DialogTheme);
        builder.setMessage("Are you sure you want to exit Ramadan Calendar and Prayer times App?")
                .setTitle("Exit")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        builder.show();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.START) == false) {
            SharedPreferences sharedPreferences = getSharedPreferences("back", MODE_PRIVATE);

            switch (sharedPreferences.getString("now", "")) {
                case "vul":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new RomjanFragment()).commit();
                    break;
                case "amol":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new RomjanFragment()).commit();
                    break;
                case "fojilot":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new RomjanFragment()).commit();
                    break;
                case "koronio":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new RomjanFragment()).commit();
                    break;
                case "romjan":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new TabFragment()).commit();
                    break;
                case "calendar":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new TabFragment()).commit();
                    break;
                case "dua":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new TabFragment()).commit();
                    break;
                case "others":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new TabFragment()).commit();
                    break;
                case "name":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OthersFragment()).commit();
                    break;
                case "kursi":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OthersFragment()).commit();
                    break;
                case "dev":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new TabFragment()).commit();
                    break;
                case "sura":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OthersFragment()).commit();
                    break;
                case "kej":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OthersFragment()).commit();
                    break;
                case "home":
                    int check = sharedPreferences.getInt("rate", 0);

                    if (check == 0) {
                        showRate();
                    } else {
                        showExit();
                    }

                    break;

            }


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        if (intent.getStringExtra("now")!=null) {
            switch (intent.getStringExtra("now")) {
                case "details":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SuraFragment()).commit();
                    break;
                case "kej_details":
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new KodorEidZakatFragment()).commit();
                    break;
            }
        }

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}

