package bytelabapps.ramadansalattime.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import bytelabapps.ramadansalattime.R;

public class DivisionActivity extends AppCompatActivity {

    Intent intent;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_division);

        sharedPreferences = getSharedPreferences("division", MODE_PRIVATE);


        if (sharedPreferences.getBoolean("location_saved", false) == true) {

            ActionBar actionBar = getSupportActionBar();
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        intent = new Intent(DivisionActivity.this, DistrictActivity.class);

        editor = sharedPreferences.edit();



    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void dhaka(View view) {
        editor.putString("division", "ঢাকা");
        editor.apply();
        startActivity(intent);
        overridePendingTransition(R.anim.left_in,R.anim.left_out);
        finish();
    }

    public void chittagong(View view) {

        editor.putString("division", "চট্টগ্রাম");
        editor.apply();
        startActivity(intent);
        overridePendingTransition(R.anim.left_in,R.anim.left_out);
        finish();
    }

    public void rajshahi(View view) {
        editor.putString("division", "রাজশাহী");
        editor.apply();
        startActivity(intent);
        overridePendingTransition(R.anim.left_in,R.anim.left_out);
        finish();
    }

    public void khulna(View view) {
        editor.putString("division", "খুলনা");
        editor.apply();
        startActivity(intent);
        overridePendingTransition(R.anim.left_in,R.anim.left_out);
        finish();
    }

    public void barishal(View view) {
        editor.putString("division", "বরিশাল");
        editor.apply();
        startActivity(intent);
        overridePendingTransition(R.anim.left_in,R.anim.left_out);
        finish();
    }

    public void sylhet(View view) {
        editor.putString("division", "সিলেট");
        editor.apply();
        startActivity(intent);
        overridePendingTransition(R.anim.left_in,R.anim.left_out);
        finish();
    }

    public void rangpur(View view) {
        editor.putString("division", "রংপুর");
        editor.apply();
        startActivity(intent);
        overridePendingTransition(R.anim.left_in,R.anim.left_out);
        finish();
    }

    public void mymensing(View view) {
        editor.putString("division", "ময়মনসিংহ");
        editor.apply();
        startActivity(intent);
        overridePendingTransition(R.anim.left_in,R.anim.left_out);
        finish();
    }


    @Override
    public void onBackPressed() {
        if (sharedPreferences.getBoolean("location_saved", false) != true) {
            finish();
        } else {
            Intent intent = new Intent(DivisionActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }


    }


}
